function octave_anim_vort_part(start,final,skip)
if nargin<1
  disp('I at least need finish filenumbers')
  return
elseif nargin<2
  disp('Assuming number given is final, start set to 1')
  final=start;
  start=1;
  skip=1.
elseif nargin<3
  disp('skip set to 1')
  skip=1;
end
%workaround broken octave
figure('Position', [0, 0, 600, 600]);
for i=start:skip:final
  %a workaround for broken rendering on octave
  %gnplot backend ignores aspect ration and colour info
  %FLTK does not render with invisible window

  %currently, use FLTK and clear the window instead of creating a new
  %so that it doesn't pop into focus all the time
  clf
  hold all
  octave_plot(i, true)
  particle_plot(i)

  fOUT=sprintf('data/vort_part%04d.png',i)
  %the size specification is needed because otherwise ffmpeg complains
  print('-S600,600', '-dpng', fOUT)
end
close all
