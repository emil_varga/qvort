%function particle_vel_stats(start, final, res_threshold)
%plots and saves the histograms of velocities
%first, last == index of first and last trc file to use
%res_threshold == maximum resolution to consider
%(the better resolved the particle is, the SMALLER the resolution, see
%load_trc_file.m)

%TODO: nearly identical to particle_acc_stats and these two should and will
%be merged
function particle_vel_stats(start, final, res_threshold)
  if(~exist('start', 'var'))
    start = 1;
  end
  if(~exist('final', 'var'))
    final=inf;
  end
  if(~exist('res_threshold', 'var'))
    res_threshold = inf;
  end
  dims = load('data/dims.log');
  binary = (dims(4) == 1);

  free_vel = [];
  trap_vel = [];
  sup_vel  = [];
  i = start;
  while (i < final)
       filename = sprintf('trc%04d.log', i);
       if(~exist(['data/', filename], 'file'))
	     break;
       end

       [~, ~, ~, u, vs, ~, ~, ~, ~, ~, traps, res] = ...
       load_trc_file(filename, binary);
   
       if(res_threshold < inf)
	 idx = res < res_threshold;
       else
	 idx = true(size(res));
       end
       trapped = traps > 0;
       trap_vel = [trap_vel; u( trapped & idx,:)]; %#ok<AGROW>
       free_vel = [free_vel; u(~trapped & idx,:)]; %#ok<AGROW>
       sup_vel  = [sup_vel; vs]; %#ok<AGROW>

       i = i+1;
  end

  size(free_vel)
  size(trap_vel)
  
  all_vel = [free_vel; trap_vel];

  %%%
  %%% plot the histograms
  %%%
  
  stats('free_vel', free_vel, [-2,2], 300);
  stats('trap_vel', trap_vel, [-10, 10], 100);
  stats('all_vel', all_vel, [-2,2], 200);
  stats('sup_vel', sup_vel, [-10,10], 100);

end

function stats(what, samples, bin_limits, Nbins)

    if(size(bin_limits,1) == 1)
        bins = linspace(bin_limits(1), bin_limits(2), Nbins);
    end
    
    s = std(samples);
    m = mean(samples);
    
    titles   = {[what, ' x-component (streamwise)'],...
                [what, ' y-component'],...
                [what, ' z-component']};
            
    filename = {[what, '_hist_x.pdf'],...
                [what, '_hist_y.pdf'],...
                [what, '_hist_z.pdf']};
            
    component = {'x', 'y', 'z'};
    
    for k=1:3
        if(size(bin_limits,1) == 3)
            bins = linspace(bin_limits(k,1), bin_limits(k,2), Nbins);
        end
        
        fprintf(['mean ', what, '_', component{k}, ': %g +/- %g\n'], m(k), s(k));
        [nn, xx] = hist((samples(:,k) - m(k))/s(k), bins);
        nn = nn/sum(nn);
        figure;
        plot(xx, log(nn), 'o');
        xlabel('(v-<v>)/std(v)');
        ylabel('log_{10} PDF(v)');
        title(titles{k}, 'Interpreter', 'none');
        
        print('-dpdf', filename{k});
    	tmp = [xx', nn', log(abs(xx')), log(nn')];
    	save('-ascii', [filename{k}(1:length(filename{k})-3), 'dat'], 'tmp');
    end
end
