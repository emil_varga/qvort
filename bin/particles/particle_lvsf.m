%calculates the lagrangian velocity structure function of velocity increments
% < [v_i(t + tau) - v_i(t)]^p >
%the average is over all particles and all (permissible) times
%if no component (i) is provided of if it's 0, magnitude differences are calculated
%ti and tf are starting and finishing time of calculation, everything outside this is ignored
function particle_lvsf(p, i, ti, tf)
  files = dir('data/trc*.log');

  dims = load('data/dims.log');
  binary = (dims(4) == 1);

  if(~exist('i', 'var'))
    i=0;
  end
  if(~exist('ti', 'var'))
    ti = 0;
  end
  if(~exist('tf', 'var'))
    tf = inf;
  end

  prefix = 'mxyz';

  vels = [];

  for k=1:length(files)
    files(k).name
    [time, ~, ~, u] = load_trc_file(files(k).name, binary);
    if(time < ti)
      continue;
    end
    if(time > tf)
      break;
    end

    vv = [];
    for j=1:size(u, 1)
	if(i > 0 && i < 4)
	  vv = [vv, u(j, i)];
	elseif(i == 0)
	  vv = [vv, sqrt(sum(u(j,:).^2))];
	end
    end
    
    vels = [vels; time, vv];
  end

  tsteps = size(vels, 1);
  nv     = size(vels, 2);
  N = 0;
  lvsf = [];
  for k=1:round(tsteps/2)
    tau = vels(1+k, 1) - vels(1,1);
    
    mvd = 0; N = 0;
    for j=1:(tsteps - k)
      mvd = mvd + sum((vels(j+k, 2:nv) - vels(j, 2:nv)).^p);
      N = N + nv - 1;
    end
    mvd = mvd / N;

    lvsf = [lvsf; tau, mvd, N];
  end

  plot(lvsf(:,1), lvsf(:,2), '-o');
  figure; plot(lvsf(:,1), lvsf(:,3));

  save('-ascii', sprintf('%clvsf_%d.dat', prefix(i+1), p), 'lvsf');
      
end
