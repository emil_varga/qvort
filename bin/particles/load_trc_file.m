%takes the filename of a trc*.log file and a flag whether the file is
%binary or ASCII and loads it
%function [time, number_of_particles, r, u, vs, a_stokes, a_dtvs,...
%	  a_vsgvs, nearest, nearesti, traps, res] =
%	  load_trc_file(filename, binary)
%r = matrix number_of_particles x 3, positions
%u = same dimensions as r, velocities
%vs = same dims as above, superlfuid velocities at particles
%a_stokes = stokes drag
%a_dtvs   = partial time derivative of vs mutiplied by rho_s/rho
%a_vsgvs  = rho_s/rho * (v_s . grad) v_s
%a_dtvs + a_vsgvs give the inertial part of particles' acceleration
%all the accelerations are matrices with 3 row components
%nearest = distance to nearest vortex particle
%nearesti = index of that vortex particle
%traps    = index of the trapping vortex particle, 0 if free
%res      = ratio of the length of the last movement step to the
%distance to the nearest vortex. Perhaps counter-intuitively, the
%resolution 'res' is SMALL when the movement is well-resolved 

%the function is very similar to bin/vortex/load_var_file.m
function [time, number_of_particles, r, u, vs, a_stokes, a_dtvs,...
	  a_vsgvs, nearest, nearesti, traps, res] = load_trc_file(filename, binary)

fid=fopen(['data/', filename]);
if fid<0
    error(['trc file ', ['data/', filename],  ' does not exist, exiting script']);
    return
end

if binary
  time=fread(fid,1,'float64');
  number_of_particles=fread(fid,1,'int');

  r(:,1) = fread(fid,number_of_particles,'float64');
  r(:,2) = fread(fid,number_of_particles,'float64');
  r(:,3) = fread(fid,number_of_particles,'float64');
  u(:,1) = fread(fid,number_of_particles,'float64');
  u(:,2) = fread(fid,number_of_particles,'float64');
  u(:,3) = fread(fid,number_of_particles,'float64');
  vs(:,1) = fread(fid,number_of_particles,'float64');
  vs(:,2) = fread(fid,number_of_particles,'float64');
  vs(:,3) = fread(fid,number_of_particles,'float64');
  a_stokes(:,1) = fread(fid,number_of_particles,'float64');
  a_stokes(:,2) = fread(fid,number_of_particles,'float64');
  a_stokes(:,3) = fread(fid,number_of_particles,'float64');
  a_dtvs(:,1) = fread(fid,number_of_particles,'float64');
  a_dtvs(:,2) = fread(fid,number_of_particles,'float64');
  a_dtvs(:,3) = fread(fid,number_of_particles,'float64');
  a_vsgvs(:,1) = fread(fid,number_of_particles,'float64');
  a_vsgvs(:,2) = fread(fid,number_of_particles,'float64');
  a_vsgvs(:,3) = fread(fid,number_of_particles,'float64');
  nearest  = fread(fid, number_of_particles, 'float64');
  nearesti = fread(fid, number_of_particles, 'int');
  traps = fread(fid,number_of_particles,'int');
  res = fread(fid,number_of_particles,'float64');
else 
  %read the time
  tline=fgetl(fid);
  dummy=textscan(tline, '%f');
  time=dummy{:};
  %how many particles
  tline=fgetl(fid);
  dummy=textscan(tline, '%d');  
  number_of_particles=dummy{:};
  %get the particles%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

  for j=1:number_of_particles
    tline=fgetl(fid);
    dummy=textscan(tline, '%f');
    dummy_vect=dummy{:};
    r(j,:)        = dummy_vect(1:3);
    u(j,:)        = dummy_vect(4:6);
    vs(j,:)       = dummy_vect(7:9);
    a_stokes(j,:) = dummy_vect(10:12);
    a_dtvs(j,:)   = dummy_vect(13:15);
    a_vsgvs(j,:)  = dummy_vect(16:18);
    nearest(j)    = dummy_vect(19);
    nearesti(j)   = dummy_vect(20);
    traps(j)      = dummy_vect(21);
    res(j)        = dummy_vect(22);
  end
end
traps = uint32(traps);
fclose(fid);
end
