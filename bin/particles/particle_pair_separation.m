%calculates the time evolution of separation of particle pairs
%no arguments
function particle_pair_separation
  files = dir('data/*.trc');

  dims = load('data/dims.log');
  binary = (dims(4) == 1);

  iseps = [];

  for i=1:length(files)
      [time, number_of_particles, r, ~, ~, ~, ~,...
       ~, ~, ~, ~, ~] = load_trc_file(files(i).name, binary);

      seps = [];
      for k=1, 2, number_of_particles
	seps = [seps, sqrt(sum((r(k+1) - r(k)).^2))];
      end
      iseps = [iseps; time, seps];
  end

  mseps = [];
  ps = size(iseps, 2);
  for k=1, size(iseps, 1)
    mseps = [mseps; isesps(k, 1), mean(iseps(k, 2:ps)), std(iseps(k, 2:ps))];
  end

  plot(mseps(:,1), mseps(:,2), '-k', mseps(:,1), mseps(:,2) + mseps(:,3), '-r',...
       mseps(:,1), mseps(:,2) - mseps(:,3), '-r');

end
