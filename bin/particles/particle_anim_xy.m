%function particle_anim_xy(start,final,skip, ds, maxres)
%plots two dimensional plots of vortex particles' position (components
%ds(1) and ds(2)) from start to final skipping every skip. maxres is a
%threshold resolution above which particles are not plotted.
%similar to particle_anim and see particle_plot_xy for more details
function particle_anim_xy(start,final,skip, ds, maxres)
if nargin<1
  disp('I at least need finish filenumbers')
  ds = [1,2];
  return
elseif nargin<2
  disp('Assuming number given is final, start set to 1')
  final=start;
  start=1;
  skip=1.
  ds = [1,2];
elseif nargin<3
  disp('skip set to 1')
  skip=1;
  ds = [1,2];
elseif nargin<4
  ds = [1,2];
end
if(~exist('maxres', 'var'))
  maxres = inf;
end
figure('visible','off');
for i=start:skip:final
  particle_plot_xy(i, ds, maxres)
  fOUT=sprintf('data/par2D%04d.jpeg',i);
  print('-djpeg',fOUT)
  close all
end
