%function particle_acc_stats(first, last, res_threshold)
%plots and saves the histograms of accelerations
%first, last == index of first and last trc file to use
%res_threshold == maximum resolution to consider
%(the better resolved the particle is, the SMALLER the resolution, see
%load_trc_file.m)
function particle_acc_stats(first, last, res_threshold)
  switch(nargin)
      case 0
          first = 1;
          last = inf; res_threshold = inf;
      case 1
          last = inf; res_threshold = inf;
      case 2
          res_threshold = inf;
      case 3
          %do nothing
      otherwise
          fprintf('usage: particle_acc_stats(first, last, res_threshold)\n');
          return
  end
     
  dims = load('data/dims.log');
  binary = (dims(4) == 1);
  
  files = dir('data/trc*.log');
  
  stokes = []; dtvs = []; vsgvs = [];
  
  for i=first:length(files)
    if(i>last)
        break;
    end
    [~, ~, ~, ~, ~, a_stokes, a_dtvs,...
	a_vsgvs, ~, ~, tr, res] = load_trc_file(files(i).name, binary);
  
    idx = res < res_threshold & (tr==0);
    %load all the quantities
    stokes = [stokes; a_stokes(idx, :)]; %#ok<AGROW>
    dtvs   = [dtvs; a_dtvs(idx, :)]; %#ok<AGROW>
    vsgvs  = [vsgvs; a_vsgvs(idx, :)]; %#ok<AGROW>
  end
  
  %plot their histograms with the helper function below
  stats('stokes', stokes, [-1.5, 1.5; -1, 1; -1, 1], 100);
  stats('dtvs',   dtvs,   [-1, 1], 100);
  stats('vsgvs',  vsgvs,  [-1, 1], 100);
  stats('acc', stokes + 0.58*(dtvs + vsgvs), [-1.5, 1.5], 100);
end

function stats(what, samples, bin_limits, Nbins)
    %handles plotting and saving of the histograms

    if(size(bin_limits,1) == 1)
        bins = linspace(bin_limits(1), bin_limits(2), Nbins);
    end
    
    s = std(samples);
    m = mean(samples);
    
    titles   = {[what, ' x-component (streamwise)'],...
                [what, ' y-component'],...
                [what, ' z-component']};
            
    filename = {[what, '_hist_x.pdf'],...
                [what, '_hist_y.pdf'],...
                [what, '_hist_z.pdf']};
            
    component = {'x', 'y', 'z'};
    
    for k=1:3
        if(size(bin_limits,1) == 3)
            bins = linspace(bin_limits(k,1), bin_limits(k,2), Nbins);
        end
        
        fprintf(['mean ', what, '_', component{k}, ': %g +/- %g\n'], m(k), s(k));
        [nn, xx] = hist((samples(:,k) - m(k))/s(k), bins);
        nn = nn/sum(nn);
        figure;
        plot(xx, log(nn), 'o');
        xlabel('(a-<a>)/std(a)');
        ylabel('log_{10} PDF(a)');
        title(titles{k}, 'Interpreter', 'none');
        
        print('-dpdf', filename{k});
        tmp = [xx', nn', log(abs(xx')), log(nn')];
	    save('-ascii', [filename{k}(1:length(filename{k})-3), 'dat'], 'tmp');
    end
end
