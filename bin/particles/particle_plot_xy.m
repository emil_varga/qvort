function particle_plot_xy(filenumber, ds, maxres)
if(~exist('ds', 'var'))
  ds = [1,2];
end
if(~exist('maxres', 'var'))
  maxres = inf;
end
filename=sprintf('trc%04d.log',filenumber);
fid=fopen(filename);
%get the dimensions information from dims.log
dims=load('./data/dims.log');
binary = dims(4)==1;
[~, ~, r, ~, ~, ~, ~, ~, ~, ~, traps, res] = load_trc_file(filename, binary);
figure('visible', 'off');
ridx = res < maxres;
fidx = (traps == 0);
tmp1 = r(fidx&ridx, :);
tmp2 = r((~fidx)&ridx, :);
plot(tmp1(:, ds(1)), tmp1(:, ds(2)), '.k',...
     tmp2(:, ds(1)), tmp2(:, ds(2)), '.r', 'MarkerSize', 10);
if (dims(2)>0.)
  axis([-dims(2)/2 dims(2)/2 -dims(2)/2 dims(2)/2]); 
else
  axis([-0.1 0.1 -0.1 0.1]);      
end
pbaspect([1,1,1]);
end
