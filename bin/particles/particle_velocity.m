function particle_velocity(particles)

  files = dir('data/trc*.log');
  ts = zeros(length(files), 1);
  vs = zeros(length(files), length(particles));
  
  dims = load('data/dims.log');
  binary = (dims(4) == 1);

  for j=1:length(files)
    [time, ~, ~, u] = load_trc_file(files(j).name, binary);
    ts = [ts; time];
     
    d = [];
    for k=1:length(particles)
      d = [d, sqrt(sum(u(particles(k), :).^2))];
    end
    vs = [vs; d];
  end
     
  figure;
  hold all
  for k=1:length(particles)
    plot(ts, vs(:,k))
  end
  hold off

end
