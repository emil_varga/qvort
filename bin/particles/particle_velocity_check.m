%function particle_velocity_check(particles, tau, rhos, periodic)
%checks the cacluated particles' velocity and positions against a
%semi-analytical solution. This function assumes one straight vortex
%oriented in the positive z-direction with x=y=0 (single_line in
%run.in). Note that the periodic summing in fortran code needs to be
%changed for this to work, the finite lines (even with one periodic
%repetition) really do not produce the same field as an infinite line
%and it leads to strong discrepancies. At least around 10 repetitions
%in the z-directions are needed and either 0 or 1 in x and y
%directions, but set 'periodic' argument acoordingly (see below)

%this functions does not relate to any reasonable physical process, it
%is merely a debugging tool

%particles == the indices of the particles we are interested on
%tau       == the stokes drag time scale
%rhos      == rho_s / rho
%periodic  == simulate periodic boundary conditions in the code?
function particle_velocity_check(particles, tau, rhos, periodic)

  files = dir('data/trc*.log');

  dims = load('data/dims.log');
  binary = (dims(4) == 1);
  ts = [];
  vs = []; rs = [];

  for j=1:length(files)
    [time, ~, r, u] = load_trc_file(files(j).name, binary);
    ts = [ts; time];
     
    vms = [];
    ds  = [];
    %only the x and y components, check that particle aren't moving in
    %z-direction manually
    for k=1:length(particles)
      vms = [vms, u(particles(k), 1:2)];
      ds  = [ds,  r(particles(k), 1:2)];
    end
    vs = [vs; vms];
    rs = [rs; ds ];
  end
 
  kappa = 9.97e-4;
  %make sure we start from 0
  [ts, idx] = sort(ts);
  vs = vs(idx,:);
  rs = rs(idx,:);
  %the RHS of differential equation for the movement towards a single
  %straight line
  f = @(t, x) [x(3:4); -x(3:4)/tau - rhos*kappa^2/4/pi^2/sqrt(sum(x(1:2).^2))^4.*x(1:2)];
  pvrs = [];
  if(exist('periodic', 'var') && periodic)
    %the same as above but with 8 straight lines around the central 1
    %due to periodic boundaries
      ff = @(t,x) [x(3:4); -x(3:4)/tau + rhos*vsgvs_periodic(x(1:2), 0.1)];
  else
      ff = f;
  end

  for k=1:length(particles)
    init = [rs(1,[2*k-1, 2*k])'; vs(1,[2*k-1, 2*k])'];
    %let matlab integrate the ODE from above numerically
    %ff dievrges during trapping what breaks the integrator, so fill
    %the rest with zeros (which are the correct values for both
    %velocity and position)
    [~, tmp] = ode45(ff, ts, init);
    tmp2 = zeros(length(ts), 4);
    tmp2(1:size(tmp,1),:) = tmp;
    pvrs = [pvrs, tmp2];
  end

  figure;
  hold all
  for k=1:length(particles)
    plot(vs(:,2*k-1), vs(:,2*k), 'o', pvrs(:,4*k-1), pvrs(:,4*k), '-*')
  end
  hold off

  figure;
  hold all
  for k=1:length(particles)
    plot(rs(:,2*k-1), rs(:,2*k), 'o', pvrs(:,4*k-3), pvrs(:,4*k-2), '-*')
  end
  hold off
end

function vgv = vsgvs_periodic(x, D)
  kappa = 9.97e-4;
  lcMat = zeros(3,3,3);
  lcMat([8 12 22]) = 1;
  lcMat([6 16 20]) = -1;
  Jvs = zeros(2,2);
  vs = zeros(2,1);

  %this needs to calculate the jacobian, so it's a bit messy
  for p1=-1:1 for p2=-1:1
    shift = [p1; p2]*D;
    r = x - shift;
    lr = sqrt(sum(r.^2));
    for i=1:2
        for k=1:2
           Jvs(k,i) = Jvs(k,i) +  kappa/2/pi*lcMat(i, 3, k)/lr^2;
           for p=1:2
               Jvs(k,i) = Jvs(k,i) - 2*kappa/2/pi*r(p)*r(k)/lr^4*lcMat(i,3,p);
           end
        end
    end
    vs = vs + kappa/2/pi/lr^2*[-r(2); r(1)];
  end; end;

  vgv = zeros(2,1);
  for k=1:2
      vgv(k) = dot(vs, Jvs(:,k));
  end
end
