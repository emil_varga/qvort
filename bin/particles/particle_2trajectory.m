%function particle_2trajectory(p, ds = [1,2], periodic = true)
%plots the trajectory of particles p (vector) but only the two
%components in ds (2-component vector)
%if periodic == true, expand the trajectory outside the periodic
%domain and plot it as a continuous one
function particle_2trajectory(p, ds, periodic)

  if(~exist('p', 'var') || isempty(p))
    error('which particles?')
  end

  if(~exist('ds', 'var'))
    ds = [1,2];
  end

  if(~exist('periodic', 'var'))
    periodic = true;
  end

  if(length(ds) ~= 2 || max(ds) > 3 || min(ds) < 1)
    error('ds must be 2 ints from 1:3, which plane to project onto');
  end

  dims = load('data/dims.log');
  binary = dims(4) == 1;
  D      = dims(2); %box size

  files = dir('data/trc*.log');

  [t, ~, r] = load_trc_file(files(1).name, binary);
  trjs = [t];
  for k = 1:length(p)
      trjs = [trjs, r(p(k), ds)];
  end

  for i = 2:length(files)
      [t, ~, r] = load_trc_file(files(i).name, binary);
      tmp = [];
      for k = 1:length(p)
	  tmp = [tmp, periodic_fix(trjs(i-1, [2*k, 2*k+1]),...
				   r(p(k), ds), D)];
      end
      trjs = [trjs; t, tmp];
  end

  figure;
  hold all;
  for k = 1:length(p)
      plot(trjs(:, 2*k), trjs(:, 2*k + 1));
  end
  hold off;
end

function xnew_fixed = periodic_fix(xold, xnew, D)
  dx = xnew - xold;
  dx = dx.*(1 - (abs(dx) > D/2).*D./abs(dx));

  xnew_fixed = xold + dx;
end
