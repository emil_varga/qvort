function particle_nearest_p(pnum)
    files = dir('data/trc*.log');
    
    dims = load('data/dims.log');
    binary = (dims(4) == 1);
    ts = []; nps = [];
    for i=1:length(files)
      [time, ~, ~, ~, ~, ~, ~,...
	  ~, nearest, ~, ~] = load_trc_file(files(i).name, binary);
  
      ts = [ts; time];
      nps = [nps; (nearest(pnum))'];
    end
    
    figure; hold all;
    for i=1:length(pnum)
        plot(ts, nps(:,i), 'o');
    end
end