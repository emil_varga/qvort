%function particle_trap_rate(trapping, threshold)
%plots the number of trapped particles as a function of time
%trapping == whether to use the trapping information in trc files
%threshold = if trapping == false, consider particle trapped when
%            closer than threshold to a vortex. This will
%            automatically include 'really' trapped particles, since
%            their distance is 0
function particle_trap_rate(trapping, threshold)

  if(~exist('trapping', 'var'))
    trapping = true;
  end

  if(exist('trapping', 'var') && ~trapping && ~exist('threshold',...
						     'var'))
    error('please provide threshold if trapping is turned off');
  end

  dims = load('data/dims.log');
  binary = (dims(4) == 1);

  files = dir('data/trc*.log');

  ts = [];
  ntrap = [];
  for i = 1:length(files)
     [time, ~, ~, ~, ~, ~, ~,...
	  ~, nearest, ~, traps] = load_trc_file(files(i).name, binary);

     ts = [ts; time];
     if(trapping)
       ntrap = [ntrap; sum(traps>0)];
     else
       ntrap = [ntrap; sum(nearest<threshold)];
     end
  end

  plot(ts, ntrap); xlabel('time (s)'); ylabel ('# trapped particles');
  out = [ts, ntrap];
  save('-ascii', 'trap_rate.dat', 'out');
end
