function mesh_vel_stat_process(filenumbers,varargin)
% 1st input argument: input the filenumber to be analysed either single or
    % mulitple files accepted
% 2nd input argument: either type 'normal' or 'super' depending on which
    % fluid component you want to analyse
close all
optargin = size(varargin,2);
%set options based on varargin
for i=1:optargin
    switch  cell2str(varargin(i))
        case 'normal'
            normal=1;
            super=0;
        case 'super'
            super=1;
            normal=0;
        otherwise
            disp('invalid option in input arguments')
            disp('aborting code')
            return
    end
end

fluid='varargin';
load ./data/dims.log;
msize=dims(3);
if (msize==0) 
  disp('mesh size is zero exiting script')
  return
end

counter=0;
vel_x=0;
vel_y=0;
vel_z=0;
for i=filenumbers
  filename=sprintf('data/mesh%03d.dat',i);
  fid=fopen(filename);
  if fid<0
    disp('mesh file does not exist, exiting script')
    return
  end
  disp(sprintf('mesh size is: %04d',msize))

  t=fread(fid,1,'float64');
  x=fread(fid,msize,'float64');
  dummy_unormx=fread(fid,msize^3,'float64');
  dummy_unormy=fread(fid,msize^3,'float64');
  dummy_unormz=fread(fid,msize^3,'float64');
  dummy_ux=fread(fid,msize^3,'float64');
  dummy_uy=fread(fid,msize^3,'float64');
  dummy_uz=fread(fid,msize^3,'float64');
  
    if super==1
        vel_x=dummy_ux;
        vel_y=dummy_uy;
        vel_z=dummy_uz;
    end
    if normal==1
        vel_x=dummy_unormx;
        vel_y=dummy_unormy;
        vel_z=dummy_unormz;
    end

  ux(counter*msize^3+1:(counter+1)*msize^3)=vel_x;
  uy(counter*msize^3+1:(counter+1)*msize^3)=vel_y;
  uz(counter*msize^3+1:(counter+1)*msize^3)=vel_z;
  counter=counter+1;
end
vcoff=7. ;
index = find(ux > vcoff);
ux(index) = [];
clear index;
index = find(uy > vcoff);
uy(index) = [];
clear index;
index = find(uz > vcoff);
uz(index) = [];
clear index ;
index = find(ux < -vcoff);
ux(index) = [];
clear index;
index = find(uy < -vcoff);
uy(index) = [];
clear index;
index = find(uz < -vcoff);
uz(index) = [];
clear index ;
save velocity.mat ux uy uz
