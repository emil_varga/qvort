function octave_plot(filenumber, nohold)
  filename=sprintf('data/var%04d.log',filenumber);
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  %get the dimensions information from dims.log
  dims=load('./data/dims.log');
  if dims(4)==1
    fid=fopen(filename);
    if fid<0
      disp('var file does not exist, exiting script')
      return
    end
    time=fread(fid,1,'float64');
    number_of_particles=fread(fid,1,'int');
    x=fread(fid,number_of_particles,'float64');
    y=fread(fid,number_of_particles,'float64');
    z=fread(fid,number_of_particles,'float64');
    f=fread(fid,number_of_particles,'int');
    u=fread(fid,number_of_particles,'float64');
  else 
    fid=fopen(filename);
    if fid<0
      disp('var file does not exist, exiting script')
      return
    end
    %read the time
    tline=fgetl(fid);
    dummy=textscan(tline, '%f');
    time=dummy{:};
    %how many particles
    tline=fgetl(fid);
    dummy=textscan(tline, '%d');  
    number_of_particles=dummy{:};
    %get the particles%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    for j=1:number_of_particles
      tline=fgetl(fid);
      dummy=textscan(tline, '%f');
      dummy_vect=dummy{:};
      x(j)=dummy_vect(1);
      y(j)=dummy_vect(2);
      z(j)=dummy_vect(3);
      f(j)=dummy_vect(4);
      u(j)=dummy_vect(5);
    end
    f=uint16(f);
  end
  %now create vectors to plot%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  ploted = zeros(size(x));
  %figure('visible', 'off');
  if(nargin == 1)
    hold all;
  end
  for j=1:number_of_particles
    if(round(f(j))==0)
      ploted(j) = 1;
    else if(ploted(j) == 0)
	   %construct the entire line
	   k = j;
	   dummy_x = [];
	   while(ploted(k) == 0)
	     ploted(k) = 1;
	     dummy_x = [dummy_x; x(k), y(k), z(k)];
	     newk = round(f(k));

	     dist = sqrt((x(k) - x(newk))^2 + (y(k) - y(newk))^2 + (z(k) - z(newk))^2);
	     if(dist > dims(2)/2)
	       break;
	     end
	     k = newk;
	   end
	   if(ploted(k) == 1 && dist < dims(2)/2) %close the loop
	     dummy_x = [dummy_x; x(k), y(k), z(k)];
	   end
	   plot3(dummy_x(:,1),dummy_x(:,2),dummy_x(:,3), 'color', 'k', 'linewidth', 2);
       clear dummy;
	 end
    end
  end
  axis([-dims(2)/2 dims(2)/2 -dims(2)/(2*dims(7))...
         dims(2)/(2*dims(7)) -dims(2)/(2*dims(7))...
         dims(2)/(2*dims(7))]);
  daspect([1 dims(7) dims(7)]);
  set(gca,'xtick',[]);
  set(gca,'ytick',[]);
  set(gca,'ztick',[]);
  grid on;
  box on;
  view(3);
  if(nargin == 1)
    hold off;
  end
end
