%takes the filename of a var*.log file and a flag whether the file is
%binary or ASCII and loads it
%[time, number_of_particles, x, y, z, f, u, u2] = load_var_file(filename, binary)
%taken verbatim from vortex_plot.m
function [time, number_of_particles, x, y, z, f, u, u2] = load_var_file(filename, binary)

fid=fopen(['data/', filename]);
if fid<0
    error(['var file ', ['data/', filename],  ' does not exist, exiting script']);
end

if binary
  time=fread(fid,1,'float64');
  number_of_particles=fread(fid,1,'int');
  x=fread(fid,number_of_particles,'float64');
  y=fread(fid,number_of_particles,'float64');
  z=fread(fid,number_of_particles,'float64');
  f=fread(fid,number_of_particles,'int');
  u=fread(fid,number_of_particles,'float64');
  u2=fread(fid,number_of_particles,'float64');
else 
  %read the time
  tline=fgetl(fid);
  dummy=textscan(tline, '%f');
  time=dummy{:};
  %how many particles
  tline=fgetl(fid);
  dummy=textscan(tline, '%d');  
  number_of_particles=dummy{:};
  %get the particles%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  x = zeros(number_of_particles, 1);
  y = zeros(number_of_particles, 1);
  z = zeros(number_of_particles, 1);
  f = zeros(number_of_particles, 1);
  u = zeros(number_of_particles, 1);
  u2 = zeros(number_of_particles, 1);
  for j=1:number_of_particles
    tline=fgetl(fid);
    dummy=textscan(tline, '%f');
    dummy_vect=dummy{:};
    x(j)=dummy_vect(1);
    y(j)=dummy_vect(2);
    z(j)=dummy_vect(3);
    f(j)=dummy_vect(4);
    u(j)=dummy_vect(5);
    u2(j)=dummy_vect(6);
  end
  f=uint16(f);
end
fclose(fid);
end
