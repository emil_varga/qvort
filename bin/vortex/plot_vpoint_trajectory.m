%plot_vpoint_trajectory(index)
%plots one of the vortex particle trajectories in data/trajectory
%trajectories are sequentially numbered from 1 and stored in separate
%trj*.log files
function plot_vpoint_trajectory(index)
    filename = sprintf('data/trajectories/trj%05d.log', index);
    
    tr = load(filename);
    %the file format is simple table
    %time x y z
    
    plot3(tr(:,2), tr(:,3), tr(:,4), 'o');
end
