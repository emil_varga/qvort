function multiple_curv_pdfs(path1,path2,path3,file1,file2,file3)
%ideally make cf path1 and ks path2 and dns path3
data_path1=strcat('./',path1,'/data/lucy_curv%04d.dat')
data_path2=strcat('./',path2,'/data/lucy_curv%04d.dat')
data_path3=strcat('./',path3,'/data/lucy_curv%04d.dat')

%check filenumber has been set
if exist('file1')==0
  disp('you have not set the first filnumber')
  disp('aborting code')
  return
end
if exist('file2')==0
  disp('you have not set the second filnumber')
  disp('aborting code')
  return
end
if exist('file3')==0
  disp('you have not set he third filnumber')
  disp('aborting code')
  return
end

filename1=sprintf(data_path1,file1)
fid1=fopen(filename1);
  if fid1<0
      disp('first lucy_curv file does not exist, exiting script')
      return
  end
time1=fread(fid1,1,'float64');
number_of_particles1=fread(fid1,1,'int');
curv_data1=fread(fid1,number_of_particles1,'float64');

%remove the zero's and generate pdf
curv_data1(curv_data1==0) = [];

[f,xi] = ksdensity(curv_data1,'support','positive');
f1=f;
xi1=xi;
figure('Name','Curvature PDF')
plot(xi,f,'r','LineWidth',2);
set(gca,'FontSize',16)
xlabel('$C$','interpreter','latex','fontsize',16)
ylabel('$PDF(C)$','interpreter','latex','fontsize',16)

%generate statisics...
curv_quantiles1 = quantile(curv_data1,[0.25 0.5 0.75])
curv_standard_deviation1 = std(curv_data1)
curv_mean1 = mean(curv_data1)

%double check there are no negative curvatures
count=0;
for i=1:size(curv_data1)
    if curv_data1(i)<0
        count=count+1;
        disp('curvature is below zero')
        curv_data1(i)
    end
end
count

filename2=sprintf(data_path2,file2)
fid2=fopen(filename2);
  if fid2<0
      disp('lucy_curv file does not exist, exiting script')
      return
  end
time2=fread(fid2,1,'float64');
number_of_particles2=fread(fid2,1,'int');
curv_data2=fread(fid2,number_of_particles2,'float64');

%remove the zero's and generate pdf
curv_data2(curv_data2==0) = [];

[f,xi] = ksdensity(curv_data2,'support','positive');
f2=f;
xi2=xi;
hold on
plot(xi,f,'k','LineWidth',2);

%generate statisics...
curv_quantiles2 = quantile(curv_data2,[0.25 0.5 0.75])
curv_standard_deviation2 = std(curv_data2)
curv_mean2 = mean(curv_data2)

%double check there are no negative curvatures
count=0;
for j=1:size(curv_data2)
    if curv_data2(j)<0
        count=count+1;
        disp('curvature is below zero')
        curv_data2(j)
    end
end
count

filename3=sprintf(data_path3,file3)
fid3=fopen(filename3);
  if fid3<0
      disp('lucy_curv file does not exist, exiting script')
      return
  end
time3=fread(fid3,1,'float64');
number_of_particles3=fread(fid3,1,'int');
curv_data3=fread(fid3,number_of_particles3,'float64');

%remove the zero's and generate pdf
curv_data3(curv_data3==0) = [];

[f,xi] = ksdensity(curv_data3,'support','positive');
f3=f;
xi3=xi;
hold on
plot(xi,f,'b','LineWidth',2);

%generate statisics...
curv_quantiles3 = quantile(curv_data3,[0.25 0.5 0.75])
curv_standard_deviation3 = std(curv_data3)
curv_mean3 = mean(curv_data3)

%double check there are no negative curvatures
count=0;
for k=1:size(curv_data3)
    if curv_data3(k)<0
        count=count+1;
        disp('curvature is below zero')
        curv_data3(k)
    end
end
count

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
figure('Name','Curvature PDF check')
plot(xi1,f1,'r','LineWidth',2);
hold on
plot(xi2,f2,'k','LineWidth',2);
hold on
plot(xi3,f3,'b','LineWidth',2);
set(gca,'FontSize',16)
xlabel('$C$','interpreter','latex','fontsize',16)
ylabel('$PDF(C)$','interpreter','latex','fontsize',16)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
figure('Name','Loglog Curvature PDF')
loglog(xi1,f1,'r','LineWidth',2);
hold on
loglog(xi2,f2,'k','LineWidth',2);
hold on
loglog(xi3,f3,'b','LineWidth',2);
set(gca,'FontSize',16)
xlabel('$log(C)$','interpreter','latex','fontsize',16)
ylabel('$log(PDF(C))$','interpreter','latex','fontsize',16)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
figure('Name','Log linear Curvature PDF')
semilogx(xi1,f1,'r','LineWidth',2);
hold on
semilogx(xi2,f2,'k','LineWidth',2);
hold on
semilogx(xi3,f3,'b','LineWidth',2);
set(gca,'FontSize',16)
xlabel('$log(C)$','interpreter','latex','fontsize',16)
ylabel('$PDF(C)$','interpreter','latex','fontsize',16)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
disp('finished')
