function curv_pdf_and_data(filenumber)
%check filenumber has been set
if exist('filenumber')==0
  disp('you have not set filnumber')
  disp('aborting code')
  return
end
filename=sprintf('data/lucy_curv%04d.dat',filenumber);
fid=fopen(filename);
  if fid<0
      disp('lucy_curv file does not exist, exiting script')
      return
  end

time=fread(fid,1,'float64');
number_of_particles=fread(fid,1,'int');
curv_data=fread(fid,number_of_particles,'float64');

%remove the zero's and generate pdf
curv_data(curv_data==0) = [];

[f,xi] = ksdensity(curv_data,'support','positive');
figure('Name','Curvature PDF')
plot(xi,f,'r','LineWidth',2);
set(gca,'FontSize',16)
xlabel('$C$','interpreter','latex','fontsize',16)
ylabel('$PDF(C)$','interpreter','latex','fontsize',16)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
figure('Name','Loglog Curvature PDF')
loglog(xi,f,'r','LineWidth',2);
set(gca,'FontSize',16)
xlabel('$C$','interpreter','latex','fontsize',16)
ylabel('$PDF(C)$','interpreter','latex','fontsize',16)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%generate statisics...
curv_quantiles = quantile(curv_data,[0.25 0.5 0.75])
curv_standard_deviation = std(curv_data)
curv_mean = mean(curv_data)

%double check there are no negative curvatures
count=0;
for i=1:size(curv_data)
    if curv_data(i)<0
        count=count+1;
        disp('curvature is below zero')
        curv_data(i)
    end
end
count
disp('finished')
