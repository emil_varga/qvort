%function all_velocities = vortex_ptv_hist(filepat, force, all_velocities)
%function to calculate vortex velocity histograms
%all_velocities = if all velocities are already loaded in matlab this can
%be used so that it's not loaded again (takes a long time)
%filepat = difference files pattern
%force = force redoing the velocities even if the dump already exists
%in 'data/'
%this functions loads and processes possibly around 100 MB, so it
%creates a dump of all velocities in data/ that can be reused which 
%slightly speeds up the process
function all_velocities = vortex_ptv_hist(filepat, force, all_velocities)

%use the dump by defaul if it exists
if(~exist('force', 'var'))
    force = false;
end

dims = load('./data/dims.log');

ts = load('./data/ts.log');
files = dir(['data/' filepat, '*']);

if(~exist('all_velocities', 'var'))
    if (force || ~exist('./data/vort_velocities_dump.dat', 'file'))
        all_velocities = [];

        for k=1:length(files)
            [file_idx, count] = sscanf(files(k).name, [filepat, '%d.log']);
            OK = count == 1;
            file_idx = int32(file_idx);
            if(~OK)
                error(['unknown file: ', files(k).name]);
            end
	    %make sure we haven't mixed up the indexing in any way
            assert(file_idx == ts(file_idx, 1));

            diffs = load(['data/' files(k).name]);
	    %this should be able to deal with adaptive timestep
            dt = ts(file_idx + 1, 2) - ts(file_idx, 2);

            okp = diffs(:,1) > 0.5;
            vx = diffs(okp, 4)/dt;
            vy = diffs(okp, 5)/dt;
            vz = diffs(okp, 6)/dt;
            clear('diffs');

            all_velocities = [all_velocities;...
                vx, vy, vz];
        end

        save('-ascii', 'data/vort_velocities_dump.dat', 'all_velocities');
    else
        fprintf('using existing dump of velocities\n');
        all_velocities = load('data/vort_velocities_dump.dat');
    end
end

%plotting of normalized histograms

stdx = std(all_velocities(:,1));
stdy = std(all_velocities(:,2));
stdz = std(all_velocities(:,3));

[nnx, xx] = hist(all_velocities(:,1)/stdx, round(sqrt(mean(ts(:,3)))));
[nny, yy] = hist(all_velocities(:,2)/stdy, round(sqrt(mean(ts(:,3)))));
[nnz, zz] = hist(all_velocities(:,3)/stdz, round(sqrt(mean(ts(:,3)))));

nnx = nnx/sum(nnx);
nny = nny/sum(nny);
nnz = nnz/sum(nnz);

gpdf = @(p, x) p(3)/sqrt(2*pi*p(1)^2)*exp(-(x-p(2)).^2/2/p(1)^2);

px0 = [std(xx), mean(xx), max(nnx)*sqrt(2*pi)*std(xx)];
py0 = [std(yy), mean(yy), max(nny)*sqrt(2*pi)*std(yy)];
pz0 = [std(zz), mean(zz), max(nnz)*sqrt(2*pi)*std(zz)];

px = nlinfit(xx, nnx, gpdf, px0);
py = nlinfit(yy, nny, gpdf, py0);
pz = nlinfit(zz, nnz, gpdf, pz0);

r = [xx' - px(2), nnx', gpdf(px, xx)'];
save('-ascii', 'data/vort_velx_histogram.dat', 'r');
figure;
semilogy(xx - px(2), nnx, 'o', xx - px(2), gpdf(px, xx)); title('velocity x-component');
xlabel('(v - <v>)/std(v)'); ylabel('PDF(v)'); legend('simulation', 'gaussian fit');
print('-deps', 'vort_vx_hist.eps');

r = [yy' - py(2), nny', gpdf(py, yy)'];
save('-ascii', 'data/vort_vely_histogram.dat', 'r');
figure;
semilogy(yy - py(2), nny, 'o', yy - py(2), gpdf(py, yy)); title('velocity y-component');
xlabel('(v - <v>)/std(v)'); ylabel('PDF(v)'); legend('simulation', 'gaussian fit');
print('-deps', 'vort_vy_hist.eps');

r = [zz' - pz(2), nnz', gpdf(pz, zz)'];
save('-ascii', 'data/vort_velz_histogram.dat', 'r');
figure;
semilogy(zz - pz(2), nnz, 'o', zz - pz(2), gpdf(pz, zz)); title('velocity z-component');
xlabel('(v - <v>)/std(v)'); ylabel('PDF(v)'); legend('simulation', 'gaussian fit');
print('-deps', 'vort_vz_hist.eps');

end
