%function vortex_ptv_trajectories()
%creates trajectories from the existing diff files (see vortex_ptv)
%and saves them in data/trajectories in separate trj*.log files for
%each trajectory

%warning: this function is an absolute mess
function vortex_ptv_trajectories()

%all the difference files
dfiles = dir('data/vpos_diff*.log');

dims = load('./data/dims.log');
binary = dims(4) == 1;

%take the first one to start the process
diffs = load(['data/', dfiles(1).name]);

trajectories = {};
Ntrj = 0;

%the var files referenced in the diff file
[old_t, ~, ox, oy, oz, ~, ~, ~] = load_var_file('var0001.log', binary);
[new_t, ~, nx, ny, nz, ~, ~, ~] = load_var_file('var0002.log', binary);            

%start building the trajectories
for i=1:size(diffs, 1)
    if(diffs(i, 1) > 0)
        oi = diffs(i, 2);
        ni = diffs(i, 3);
        Ntrj = Ntrj + 1;
	%first element is a trajectory index, 2nd is at which vortex
	%we currently are (index to arrays returned by load_var_file)
	%the third is a matrix with time dependance of position and
	%vortex particle inde along the trajectory
        trajectories(Ntrj, :) = {Ntrj, diffs(i, 3), [old_t, ox(oi), oy(oi), oz(oi), diffs(i, 2);...
                                                     new_t, nx(ni), ny(ni), nz(ni), diffs(i, 3)]};
    end
end

clear('old_t', 'new_t', 'ox', 'oy', 'oz', 'nx', 'ny', 'nz');

%we will be saving stuff here
mkdir('data', 'trajectories');


old_file_idx = 1;

%start from 2, because we already dealt with the first one to start
%the whole process
for k = 2:length(dfiles)
    file_idx = sscanf(dfiles(k).name, 'vpos_diff_%d.log');
    %make sure we are moving one by one
    assert(file_idx == old_file_idx + 1);
    
    diffs = load(['data/' dfiles(k).name]);
    varfile = sprintf('var%04d.log', file_idx+1);
    [t, ~, x, y, z, ~, ~, ~] = load_var_file(varfile, binary);
    
    curr_idxs = cell2mat(trajectories(:,2));        
    old_idxs = diffs(:,2);
    
    %are there any indexes in the new diff file that connect to the
    %current vortex particle indices?
    ongoing = ismember(old_idxs, curr_idxs);
    
    for i=1:size(diffs, 1)
	%debugging
        %assert(all(unique(curr_idxs, 'stable') == curr_idxs));
        %assert(all(unique(old_idxs(diffs(:,1) > 0), 'stable') == old_idxs(diffs(:,1) > 0)));
        
        if(ongoing(i)) %a point for an existing trajectory
            trj = find(curr_idxs == diffs(i, 2)); %which trajectory are we working with?
                                    
            if(~isempty(trj))
                if(diffs(i,1) > 0.5) %the connection was OK, add the point            
		    %diffs(i,1) is 0 when connection is not OK and 1
		    %if it is

		    %the new indices in the trajectory
                    nidx = diffs(i, 3);                            
                    trajectories{trj, 3} = [trajectories{trj, 3};...
                                            t, x(nidx), y(nidx), z(nidx), nidx];

                    trajectories{trj, 2} = nidx;
                else %the connection is uncertain, terminate trajectory and save
                    save_trajectory(trajectories(trj, :));
                    trajectories(trj, :) = [];
                    curr_idxs = cell2mat(trajectories(:,2));               
                end
            end
        else %a new point
            if(diffs(i, 1) > 0) %the connection was OK, start new trajectory
                Ntrj = Ntrj + 1;
                varfile = sprintf('var%04d.log', file_idx); %load the old file
                [old_t, ~, ox, oy, oz, ~, ~, ~] = load_var_file(varfile, binary);
                
		%new and old indices
                nidx = diffs(i, 3);
                oidx = diffs(i, 2);
                
                trajectories(size(trajectories, 2) + 1, :) =...
                    {Ntrj, diffs(i, 3), [old_t, ox(oidx), oy(oidx), oz(oidx), oidx;...
                                         t, x(nidx), y(nidx), z(nidx), nidx]};
            end
        end
    end
    old_file_idx = file_idx;
end

%save all trajectories that are left
for k=1:size(trajectories, 2)
    save_trajectory(trajectories(k,:));
end

end

function save_trajectory(tr)
    if(size(tr{3}, 1) < 50) %don't save very short trajectories
        return;
    end
    persistent filei;
    if(isempty(filei))
        filei = 0;
    end
    
    filei = filei + 1;
    fn = sprintf('trj%05d.log', filei);
    fn = ['data/trajectories/', fn];
    fid = fopen(fn, 'w');
    fprintf(fid, '%%t \t x \t y \t z \t idx\n');
    fclose(fid);
    
    r = tr{3};
    save('-ascii', '-append', fn, 'r');
end
