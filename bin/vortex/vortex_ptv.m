%tracks the vortex points by
%vortex_plot(dnear_r, dfar_r)
%     dnear_r, dfar_r -- sets which points are accepted as successive
%     the criterion is (d1, d2 are first and second nearest neihbours and
%     avg_d is mean distance between points)
%      d1 < avg_d*dnear_r && d2 > avg_d*dfar_r
function vortex_ptv(dnear_r, dfar_r)

dims = load('./data/dims.log');
delta = dims(1);
binary = dims(4)==1;

ts = load('./data/ts.log');
files = dir('./data/var*.log');

start_idx = 1;

%skip any files that aren't var<NUMBER>.log
for k=1:length(files)
    [old_file_idx, OK] = str2num(files(k).name(4:7));
    old_file_idx = int32(old_file_idx);
    if(~OK)
        continue;
    end
    
    %if we have a var<NUMBER>.log file, use it as a starting point
    [~, ~, x, y, z, f, ~, ~] = load_var_file(files(k).name, binary);
    start_idx = start_idx + 1;
    if(OK) 
        break;
    end
end
%only consider non-empty vortex points
idx = f > 0;
x = x(idx); y = y(idx); z = z(idx);

%use the matlab KD tree for searching
stree = KDTreeSearcher([x, y, z]);

for k=start_idx:length(files)
    [file_idx, OK] = str2num(files(k).name(4:7));
    %check again if we have a correct file
    if(OK)
        file_idx = int32(file_idx);
	%make sure we aren't skipping anything
        assert(file_idx == old_file_idx + 1);
        [~, ~, x, y, z, f, ~, ~] = load_var_file(files(k).name, binary);
        
        fidx = f > 0;
        
	%the diff file is an ASCII file with the following format
        fnout = sprintf('./data/vpos_diff_%04d.log', old_file_idx);
        fid = fopen(fnout, 'w');
	%old and new idx are the indices in the f array of the fortran
	%code corresponding to vortex particles on the same trajectory
	%in vast majoriy of cases old_idx == new_idx, unless remeshing
	%or reconnection occured
	%dx, dy, dz is the displacement between two var* files, d1, d2
	%are the nearest and second-nearest distances to vortex points
	%and df is the distance to the particle in front of this one
	%the first OK column shows whether we are sure this is a
	%correct connection in the trajectory according to criterion
	%mentioned at the top
        fprintf(fid, '%%OK \t old_idx \t new_idx \t dx \t dy \t dz \t d1 \t d2 \t df\n');
        fclose(fid);
        
        [idx, d] = knnsearch(stree, [x(fidx), y(fidx), z(fidx)], 'K', 2);
        
        assert(file_idx == ts(file_idx, 1));
        forward_d = sqrt((x(fidx) - x(f(fidx))).^2 + (y(fidx) - y(f(fidx))).^2 + (z(fidx) - z(f(fidx))).^2);
        
        dx = x(fidx) - stree.X(idx(:,1), 1);
        dy = y(fidx) - stree.X(idx(:,1), 2);
        dz = z(fidx) - stree.X(idx(:,1), 3);
        new_idx = (1:length(x));
        new_idx = new_idx(fidx)';
        
        clear('stree');
        
	%track the progress
        fprintf('file: \t %s\n', files(k).name);
        
        okp = d(:,1) < forward_d*dnear_r & d(:,2) > forward_d*dfar_r;
                
        r = [okp, idx(:,1), new_idx, dx, dy, dz, d(:,1)/delta, d(:,2)/delta, forward_d/delta];
        save('-ascii', '-append', fnout, 'r');
        clear('r');
        
        old_file_idx = file_idx;
        stree = KDTreeSearcher([x, y, z]);
	%matlab was complaining about memory exhaustion without this
        clear('x', 'y', 'z', 'f', 'fidx');
    end
end

end
