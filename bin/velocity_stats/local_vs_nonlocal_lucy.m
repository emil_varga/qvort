%function local_vs_nonlocal_lucy(path1,path2)
function local_vs_nonlocal_lucy(path1,path2,path3)
%ideally make cf path1 and ks path2 and if there dns path3
data_path1=strcat('./',path1,'/data/local_v_nonlocal.log')
data_path2=strcat('./',path2,'/data/local_v_nonlocal.log')
data_path3=strcat('./',path3,'/data/local_v_nonlocal.log')
A1=load(data_path1);
A2=load(data_path2);
A3=load(data_path3);

figure('Name','v^{non}/v^{loc}')
plot(A1(:,1),A1(:,6)./A1(:,3),'r','LineWidth',2)
hold on
plot(A2(:,1),A2(:,6)./A2(:,3),'k','LineWidth',2)
%comment if no 3rd path
hold on
plot(A3(:,1),A3(:,6)./A3(:,3),'b','LineWidth',2)
%%%%
set(gca,'FontSize',16)
xlabel('$t$','interpreter','latex','fontsize',16)
ylabel('$v^{non}/v^{loc}$','interpreter','latex','fontsize',20)

figure('Name','v^{loc}/v^{si}')
plot(A1(:,1),A1(:,3)./(A1(:,3)+A1(:,6)),'r','LineWidth',2)
hold on
plot(A2(:,1),A2(:,3)./(A2(:,3)+A2(:,6)),'k','LineWidth',2)
%comment if no 3rd path
hold on
plot(A3(:,1),A3(:,3)./(A3(:,3)+A3(:,6)),'b','LineWidth',2)
%%%
set(gca,'FontSize',16)
xlabel('$t$','interpreter','latex','fontsize',16)
ylabel('$v^{loc}/v^{si}$','interpreter','latex','fontsize',20)

figure('Name','v^{non}/v^{si}')
plot(A1(:,1),A1(:,6)./(A1(:,3)+A1(:,6)),'r','LineWidth',2)
hold on
plot(A2(:,1),A2(:,6)./(A2(:,3)+A2(:,6)),'k','LineWidth',2)
%comment if no 3rd path
hold on
plot(A3(:,1),A3(:,6)./(A3(:,3)+A3(:,6)),'b','LineWidth',2)
%%%%
set(gca,'FontSize',16)
xlabel('$t$','interpreter','latex','fontsize',16)
ylabel('$v^{non}/v^{si}$','interpreter','latex','fontsize',20)

%print our average nonlocal/total proportion
disp('counterlow nonlocal over total average is:')
%mean(A1(250:875,6)./(A1(250:875,3)+A1(250:875,6)))
mean(A1(:,6)./(A1(:,3)+A1(:,6)))
disp('ks nonlocal over total average is:')
%mean(A2(250:875,6)./(A2(250:875,3)+A2(250:875,6)))
mean(A2(:,6)./(A2(:,3)+A2(:,6)))
disp('dns nonlocal over total average is:')
%mean(A3(400:1400,6)./(A3(400:1400,3)+A3(400:1400,6)))
mean(A3(:,6)./(A3(:,3)+A3(:,6)))