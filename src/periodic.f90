!>all the routines used to keep the code periodic
module periodic
  use cdata
  use general
  contains 
  !******************************************************************
  !>dummy routine, calls get_ghost_p below
  subroutine ghostp
    implicit none
    integer :: i
    !$omp parallel do private(i) shared(f)
    do i=1, pcount
      if (f(i)%infront==0) cycle !empty particles
      call get_ghost_p(i,f(i)%ghosti, f(i)%ghostb,f(i)%ghostii, f(i)%ghostbb)
    end do
    !$omp end parallel do
  end subroutine
  !******************************************************************
  !>set the ghost particles, essentially these are the positions of the
  !!particles infront/behind and twice infront/behind
  !!if they are at the other side of the box 
  !!due to periodic b.c. this position must be adjusted
  subroutine get_ghost_p(i,ginfront,gbehind,giinfront,gbbehind)
    implicit none
    integer, intent(IN) :: i
    real, intent(out) :: ginfront(3), gbehind(3)
    real, intent(out) :: giinfront(3), gbbehind(3)
    
    ginfront  = f(f(i)%infront)%x
    gbehind   = f(f(i)%behind)%x
    giinfront = f(f(f(i)%infront)%infront)%x
    gbbehind  = f(f(f(i)%behind)%behind)%x
    !if periodic then must do more in here
    if (periodic_bc) then
       !we must ensure that ginfront/gbehind is not on the other side of the box
       call coerce2(f(i)%x, ginfront,  box_size/2)
       call coerce2(f(i)%x, gbehind,   box_size/2)
       call coerce2(f(i)%x, giinfront, box_size/2)
       call coerce2(f(i)%x, gbbehind,  box_size/2)
    else if (periodic_bc_notx) then
       call coerce2(f(i)%x, ginfront,  box_size/2, [2,3])
       call coerce2(f(i)%x, gbehind,   box_size/2, [2,3])
       call coerce2(f(i)%x, giinfront, box_size/2, [2,3])
       call coerce2(f(i)%x, gbbehind,  box_size/2, [2,3])
    else if (periodic_bc_notxy) then
       call coerce2(f(i)%x, ginfront,  box_size/2, [3])
       call coerce2(f(i)%x, gbehind,   box_size/2, [3])
       call coerce2(f(i)%x, giinfront, box_size/2, [3])
       call coerce2(f(i)%x, gbbehind,  box_size/2, [3])
    end if

    if (mirror_bc) then
      if (f(i)%pinnedi) then
        if (f(i)%x(1)>(box_size/2.-delta))  ginfront(1)=box_size-ginfront(1)
        if (f(i)%x(1)<-(box_size/2.-delta)) ginfront(1)=-box_size-ginfront(1)
        if (f(i)%x(2)>(box_size/2.-delta))  ginfront(2)=box_size-ginfront(2)
        if (f(i)%x(2)<-(box_size/2.-delta)) ginfront(2)=-box_size-ginfront(2)
        if (f(i)%x(3)>(box_size/2.-delta))  ginfront(3)=box_size-ginfront(3)
        if (f(i)%x(3)<-(box_size/2.-delta)) ginfront(3)=-box_size-ginfront(3)
      end if
      if (f(i)%pinnedb) then
        if (f(i)%x(1)>(box_size/2.-delta))  gbehind(1)=box_size-gbehind(1)
        if (f(i)%x(1)<-(box_size/2.-delta)) gbehind(1)=-box_size-gbehind(1)
        if (f(i)%x(2)>(box_size/2.-delta))  gbehind(2)=box_size-gbehind(2)
        if (f(i)%x(2)<-(box_size/2.-delta)) gbehind(2)=-box_size-gbehind(2)
        if (f(i)%x(3)>(box_size/2.-delta))  gbehind(3)=box_size-gbehind(3)
        if (f(i)%x(3)<-(box_size/2.-delta)) gbehind(3)=-box_size-gbehind(3)
      end if
    end if
  end subroutine
  !******************************************************************
  !>if a point/particle leaves one side of the box, 
  !>reinsert it on the opposite side
  subroutine enforce_periodic()
    implicit none
    integer :: i
    !$omp parallel do private(i)
    do i=1, pcount
      if (f(i)%infront==0) cycle !empty particle
      call coerce(f(i)%x, box_size/2)
      if(f(i)%trapped_particle > 0) then
         p(f(i)%trapped_particle)%x = f(i)%x
      end if
   end do
    !$omp end parallel do
  end subroutine
  !******************************************************************
  !>if a point/particle leaves one side of the box, 
  !>reinsert it on the opposite side - do not do the x direction
  !> in the x direction we simple remove the full loop
  subroutine enforce_periodic_yz()
    implicit none
    integer :: i
    !$omp parallel do private(i)
    do i=1, pcount
      if (f(i)%infront==0) cycle !empty particle
      !------------ y, z-------------------
      call coerce(f(i)%x, box_size/2, [2,3])
      if(f(i)%trapped_particle > 0) then
         p(f(i)%trapped_particle)%x = f(i)%x
      end if

      !-------------x------------------     
      if (f(i)%x(1)>(box_size/2.)) then
        call boundary_loop_remove(i)
      else if (f(i)%x(1)<(-box_size/2.)) then
        call boundary_loop_remove(i)
      end if
    end do
    !$omp end parallel do
  end subroutine
  !******************************************************************
  !>if a point/particle leaves one side of the box, 
  !>reinsert it on the opposite side - do not do the x/y direction
  !> in the x/y direction we simple remove the full loop
  subroutine enforce_periodic_z()
    implicit none
    integer :: i
    !$omp parallel do private(i)
    do i=1, pcount
      if (f(i)%infront==0) cycle !empty particle
      !-------------x------------------     
      if (f(i)%x(1)>(box_size/2.)) then
        call boundary_loop_remove(i)
      else if (f(i)%x(1)<(-box_size/2.)) then
        call boundary_loop_remove(i)
      end if
      !-------------y------------------
      if (f(i)%x(2)>(box_size/2.)) then
        call boundary_loop_remove(i)
      else if (f(i)%x(2)<(-box_size/2.)) then
        call boundary_loop_remove(i)
      end if
      !-------------z------------------
      call coerce(f(i)%x, box_size/2, [3])
      if(f(i)%trapped_particle > 0) then
         p(f(i)%trapped_particle)%x = f(i)%x
      end if
    end do
    !$omp end parallel do
  end subroutine
  !******************************************************************
  !> remove loops which hit the boundaries
  subroutine enforce_open_removal()
    implicit none
    integer :: i
    do i=1, pcount
      if (f(i)%infront==0) cycle !empty particle
      !-------------x------------------     
      if (f(i)%x(1)>(box_size/2.)) then
        call boundary_loop_remove(i)
      else if (f(i)%x(1)<(-box_size/2.)) then
        call boundary_loop_remove(i)
      end if
      !-------------y------------------
      if (f(i)%x(2)>(box_size/(2.*xdim_scaling_factor))) then
        call boundary_loop_remove(i)
      else if (f(i)%x(2)<(-box_size/(2.*xdim_scaling_factor))) then
        call boundary_loop_remove(i)
      end if
      !-------------z------------------
      if (f(i)%x(3)>(box_size/(2.*xdim_scaling_factor))) then
        call boundary_loop_remove(i)
      else if (f(i)%x(3)<(-box_size/(2.*xdim_scaling_factor))) then
        call boundary_loop_remove(i)
      end if
    end do
  end subroutine
  !**************************************************
  !>remove loops that have left the box in \pm x/y directions
  !>this is very similar to loop_killer in line.f90
  !!a better way would be have a separate loop count
  !!and loop removal routine in general.mod
  subroutine boundary_loop_remove(particle)
    implicit none
    integer :: particle, next
    integer :: store_next
    integer :: i
    next=particle 
    boundary_loop_remove_count=boundary_loop_remove_count+1
    do i=1, pcount
      store_next=f(next)%infront
      if (store_next/=particle) then
        boundary_loop_remove_length=boundary_loop_remove_length&
                                   +distf(next,store_next)
      end if
      call clear_particle(next) !general.mod
      next=store_next
      if (next==particle) then
        exit  
      end if
    end do
  end subroutine  
end module
