!>biot-savart routines are contained in this module
!>this was split off from timestep.mod so that tree.mod can use it
module BS
  use cdata
  use general

  contains
  !**************************************************************************
  !>the desingularised biot savart integral 
  !>\f[
  !>\frac{d\mathbf{s}_i}{dt}=\frac{\Gamma}{4\pi} \ln \left(\frac{\sqrt{\ell_i
  !>\ell_{i+1}}}{a}\right)\mathbf{s}_i' \times \mathbf{s}_i'' 
  !>+\frac{\Gamma}{4 \pi} \oint_{\cal L'} \frac{(\mathbf{s}_i-\mathbf{r}) }
  !>{\vert \mathbf{s}_i - \mathbf{r} \vert^3}
  !>\times {\bf d}\mathbf{r}
  !>\f] note the LIA part is calculated in calc_velocity
  subroutine biot_savart(i,u)
    implicit none
    integer, intent(IN) :: i !the particle we want the velocity at
    real :: u(3) !the velocity at i (non-local)#
    real :: u_bs(3) !helper array
    real :: a_bs, b_bs, c_bs !helper variables
    integer :: j !needed to loop over all particles
    do j=1, pcount
      !check that the particle is not empty/i/f(i)%behind
      if ((f(j)%infront==0).or.(i==j).or.(f(i)%behind==j)) cycle
      a_bs=distfsq(j,i) !distance squared between i and j
      b_bs=2.*dot_product((f(j)%x-f(i)%x),(f(j)%ghosti-f(j)%x))
      c_bs=dist_gen_sq(f(j)%ghosti,f(j)%x) !distance sqd between j, j+1
      !add non local contribution to velocity vector
      if (abs(4*a_bs*c_bs-b_bs**2) < epsilon(0d0)) cycle !avoid 1/0
      u_bs=cross_product((f(j)%x-f(i)%x),(f(j)%ghosti-f(j)%x))
      u_bs=u_bs*quant_circ/((2*pi)*(4*a_bs*c_bs-b_bs**2))
      u_bs=u_bs*((2*c_bs+b_bs)/sqrt(a_bs+b_bs+c_bs)-(b_bs/sqrt(a_bs)))
      u=u+u_bs !add on the non-local contribution of j
    end do
  end subroutine
  !**************************************************************************
  !>as above but shifts the particles (by a vector shift) for periodicity
  subroutine biot_savart_shift(i,u,shift)
    implicit none
    integer, intent(IN) :: i !the particle we want the velocity at
    real :: u(3) !the velocity at i (non-local)#
    real :: u_bs(3) !helper array
    real :: a_bs, b_bs, c_bs !helper variables
    real :: shift(3) !moves all the particles for periodic_bc
    integer :: j !needed to loop over all particles
    do j=1, pcount
      !check that the particle is not empty or the particle behind
      !if ((f(j)%infront==0).or.(i==j).or.(f(i)%behind==j)) cycle
      if ((f(j)%infront==0).or.(f(i)%behind==j)) cycle
      a_bs=dist_gen_sq(f(i)%x,f(j)%x+shift) !distance squared between i and j+shift
      b_bs=2.*dot_product((f(j)%x+shift-f(i)%x),(f(j)%ghosti-f(j)%x))
      c_bs=dist_gen_sq(f(j)%ghosti,f(j)%x) !distance sqd between j, j+1
      !add non local contribution to velocity vector
      if (abs(4*a_bs*c_bs-b_bs**2) < epsilon(0d0)) cycle !avoid 1/0
      u_bs=cross_product((f(j)%x+shift-f(i)%x),(f(j)%ghosti-f(j)%x))
      u_bs=u_bs*quant_circ/((2*pi)*(4*a_bs*c_bs-b_bs**2))
      u_bs=u_bs*((2*c_bs+b_bs)/sqrt(a_bs+b_bs+c_bs)-(b_bs/sqrt(a_bs)))
      u=u+u_bs !add on the non-local contribution of j
    end do
  end subroutine
  !**************************************************************************
  !>calculate the velocity field at a point x induced by the vortices now using
  !>more general biot savart integral
  !>\f[\frac{\Gamma}{4 \pi} \oint_{\cal L} \frac{(\mathbf{s}_i-\mathbf{r}) }
  !>{\vert \mathbf{s}_i - \mathbf{r} + \epsilon \vert^3}
  !>\times {\bf d}\mathbf{r}.
  !>\f]
  !>This is only used to calculate the velocity at points on a cartesian mesh.
  subroutine biot_savart_general(x,u)
    implicit none
    real, intent(IN) :: x(3)
    real, intent(OUT) :: u(3)
    real :: u_bs(3) !helper vector
    real :: a_bs, b_bs, c_bs !helper variables
    integer :: i
    do i=1, pcount
      !check that the particle is not empty
      if (f(i)%infront==0) cycle
      a_bs=dist_gen_sq(x,f(i)%x) !distance squared between x and particle
      b_bs=2.*dot_product((f(i)%x-x),(f(i)%ghosti-f(i)%x))
      c_bs=dist_gen_sq(f(i)%ghosti,f(i)%x) !distance sqd between i, i+1
      !add non local contribution to velocity vector
      if (abs(4*a_bs*c_bs-b_bs**2) < epsilon(0d0)) cycle !avoid 1/0!
      u_bs=cross_product((f(i)%x-x),(f(i)%ghosti-f(i)%x))
      u_bs=u_bs*quant_circ/((2*pi)*(4*a_bs*c_bs-b_bs**2))
      u_bs=u_bs*((2*c_bs+b_bs)/sqrt(a_bs+b_bs+c_bs)-(b_bs/sqrt(a_bs)))
      u=u+u_bs !add on the non-local contribution of i
    end do
  end subroutine

  !**************************************************************************
  !>calculate the mat. derivative of the velocity field at a point x induced
  !>by the vortices now using more general biot savart integral
  !>\f[\frac{\Gamma}{4 \pi} \oint_{\cal L} \frac{(\mathbf{s}_i-\mathbf{r}) }
  !>{\vert \mathbf{s}_i - \mathbf{r} + \epsilon \vert^3}
  !>\times {\bf d}\mathbf{r}.
  !>\f]
  subroutine bs_mat_diff(x, dtvs, vs_g_vs, vs, periodic, cutoff, lf, lpc, odkvi, oskip)
    implicit none
    real, intent(in) :: x(3)
    real, intent(inout) :: dtvs(3), vs_g_vs(3), vs(3)
    real, intent(in), optional :: cutoff
    logical, intent(in), optional :: periodic
    type(qvort), intent(in), optional, target :: lf(:)
    integer, intent(in), optional :: lpc, oskip

    type(qvort), pointer :: pf(:)
    integer :: lpcount, skip

    real, intent(out), optional :: odkvi(3,3)

    real :: Rj(3), Rjp1(3), dkvi(3, 3), RjxRjp1(3), uj(3), ujp1(3)
    real :: ujxRjp1(3), Rjxujp1(3)
    real :: lRj, lRjp1, RjRjp1, ujRj, ujRj_p1
    real :: denom

    real :: tmp1(3), tmp2(3), ek(3), shift(3)

    integer :: j, k, perx, pery, perz
    integer :: px_l, px_h, py_l, py_h, pz_l, pz_h

    if(present(lf)) then
      pf => lf
    else
      pf => f
    end if

    if(.not.present(lpc) .and. .not.present(lf)) then
      lpcount = pcount
    elseif(.not.present(lpc)) then
      lpcount = size(lf)
    else
      lpcount = lpc
    end if

    if(present(periodic) .and. periodic) then
       px_l = -1; py_l = -1; pz_l = -1;
       px_h =  1; py_h =  1; pz_h =  1;
    else
       px_l =  0; py_l =  0; pz_l =  0;
       px_h =  0; py_h =  0; pz_h =  0;
    end if
    
    !which point to avoid calculating for the trapped particles
    if(present(oskip)) then
       skip = oskip
    else
       skip = 0
    end if

    dkvi = 0
    do j = 1, lpcount
       if(pf(j)%infront == 0) cycle
       if(pf(j)%infront == skip .or. f(pf(j)%infront)%behind == skip) cycle

       do perx=px_l, px_h; do pery=py_l, py_h; do perz=pz_l, pz_h
          shift = [perx, pery, perz]*box_size

          Rj   = pf(j)%x - x + shift
          Rjp1 = pf(j)%ghosti - x + shift
          lRj   = vector_norm(Rj)
          lRjp1 = vector_norm(Rjp1)

          if(present(cutoff) .and. (lRj < cutoff .or. lRjp1 < cutoff)) cycle

          uj   = pf(j)%u
          ujp1 = f(pf(j)%infront)%u

          RjRjp1 = dot_product(Rj, Rjp1);
          RjxRjp1 = cross_product(Rj, Rjp1)

          ujxRjp1 = cross_product(uj, Rjp1);
          Rjxujp1 = cross_product(Rj, ujp1);

          ujRj = dot_product(uj, Rj);
          ujRj_p1 = dot_product(ujp1, Rjp1);

          denom = lRj*lRjp1*(lRj*lRjp1 + RjRjp1)

          !from book "quantized vortex dynamics and superfluid turbulence"
          ! D. C. Samuels paper
          tmp1 = quant_circ/4/pi*(lRj + lRjp1)*RjxRjp1 / denom
          vs = vs + tmp1

          !the Jacobian of v_s \frac{\partial^k v_s^i}{\partial x^k}
          ek = [1, 0, 0]
          do k = 1, 3
             tmp1 = (lRj + lRjp1)/denom*cross_product(ek, Rj - Rjp1)
             ek = cshift(ek, -1) 

             tmp2 = 2*Rj(k)*lRjp1**2 + 2*lRj**2*Rjp1(k) + lRj*lRjp1*(Rj(k) + Rjp1(k)) +&
                  & RjRjp1*(Rj(k)/lRj*lRjp1 + lRj*Rjp1(k)/lRjp1)
             tmp2 = (lRj + lRjp1)*tmp2/denom**2;

             tmp2 = tmp2 - (Rj(k)/lRj + Rjp1(k)/lRjp1)/denom

             tmp2 = tmp2*RjxRjp1

             dkvi(k,:) = dkvi(k,:) + quant_circ/4/pi*tmp1 + quant_circ/4/pi*tmp2
          end do

          !partial time derivative
          tmp1 = (ujRj/lRj + ujRj_p1/lRjp1)*RjxRjp1 + (lRj + lRjp1)&
               &*(ujxRjp1 + Rjxujp1);
          tmp1 = tmp1/denom;

          tmp2 = (lRj + lRjp1)*RjxRjp1/denom**2;
          tmp2 = tmp2*(2*ujRj*lRjp1**2 + 2*lRj**2*ujRj_p1 +&
               &       RjRjp1*(ujRj/lRj*lRjp1 + ujRj_p1*lRj/lRjp1) +&
               &       lRj*lRjp1*(dot_product(uj, Rjp1) + &
               &                  dot_product(Rj, ujp1)))
          dtvs = dtvs + quant_circ/4/pi*(tmp1 + tmp2)
       end do; end do; end do       
    end do

    !conevctive derivative
    do j=1, 3
      do k=1,3
        vs_g_vs(j) = vs_g_vs(j) + vs(k)*dkvi(k, j)
      end do
    end do
    if(present(odkvi)) odkvi = dkvi

  end subroutine bs_mat_diff


  !**************************************************************************
  !>as above but allows a shift for periodicity
  !>\todo makes above redundent so look into removal
  subroutine biot_savart_general_shift(x,u,shift)
    implicit none
    real, intent(IN) :: x(3)
    real, intent(OUT) :: u(3)
    real :: u_bs(3) !helper vector
    real :: shift(3) !moves all the particles for periodic_bc
    real :: a_bs, b_bs, c_bs !helper variables
    integer :: i
    do i=1, pcount
      !check that the particle is not empty
      if (f(i)%infront==0) cycle
      a_bs=dist_gen_sq(x,f(i)%x+shift) !distance squared between x and particle
      b_bs=2.*dot_product((f(i)%x+shift-x),(f(i)%ghosti-f(i)%x))
      c_bs=dist_gen_sq(f(i)%ghosti,f(i)%x) !distance sqd between i, i+1
      !add non local contribution to velocity vector
      if (abs(4*a_bs*c_bs-b_bs**2) < epsilon(0d0)) cycle !avoid 1/0!
      u_bs=cross_product((f(i)%x+shift-x),(f(i)%ghosti-f(i)%x))
      u_bs=u_bs*quant_circ/((2*pi)*(4*a_bs*c_bs-b_bs**2))
      u_bs=u_bs*((2*c_bs+b_bs)/sqrt(a_bs+b_bs+c_bs)-(b_bs/sqrt(a_bs)))
      u=u+u_bs !add on the non-local contribution of i
    end do
  end subroutine
 
end module
