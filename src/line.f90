!>all routines which alter the geometry of the vortex filament/flux tube should
!>be contained in this module.
!>The main routines here insert and remove particles to maintain a roughly
!>constant resolution along the filaments.
module line
  use cdata
  use general
  use periodic
  use reconnection
  contains
  !>insert new points to maintain the resolution of the line,
  !>this will mean the point separation lies between \f$\delta\f$
  !>and \f$\delta/2\f$ to ensure the curvature is not affected by this the new point
  !>\f$ \mathbf{s}_{i'} \f$ is positioned between i, i+1 at position
  !>\f[ \mathbf{s}_{i'}=\frac{1}{2}(\mathbf{s}_i+\mathbf{s}_{i+1})+\left( \sqrt{R^2_{i'}
  !!-\frac{1}{4}\ell_{i+1}^2}-R_{i'} \right)\frac{\mathbf{s}_{i'}''}{|\mathbf{s}_{i'}''|},\f]
  !! where \f$R_{i'}=|\mathbf{s}_{i'}''|^{-1}\f$.
  recursive subroutine pinsert
    implicit none
    type(qvort), allocatable, dimension(:) :: tmp
    real :: disti, curv, f_ddot(3)
    integer :: par_new
    integer :: old_pcount, i, inserted

    inserted = 0

    !check that we have particles
    if (particles_only.eqv..false.) then
      if (count(mask=f(:)%infront>0)==0) then
        call fatal_error('line.mod:pinsert','vortex line length is 0, run over')
      end if
    else
     return !we don't need to run this routine if pcount=0
    end if
    old_pcount=pcount
    total_length=0. !zero this
    do i=1, old_pcount
      if (i>size(f)) then
        !bug check
        print*, 'I think there is a problem' ; exit
      end if
      if (f(i)%infront==0) cycle !empty particle
      if (mirror_bc.and.f(i)%pinnedi) cycle !pinned particle
      !get the distance between the particle and the one infront
      disti=dist_gen(f(i)%x,f(i)%ghosti) !general.f90
      total_length=total_length+disti !measure total length of filaments
      if (delta_adapt) then
        curv=curvature(i)
        f(i)%delta=1.+3.*exp(50.*(-delta*curv/2.))
      else
        f(i)%delta=1. !set the prefactor to 1
      end if
      if(disti > 5*f(i)%delta*delta) then
         print *, 'suspciciously long segment', disti, i, f(i)%delta*delta
         call abort
      end if
      if (disti>f(i)%delta*delta) then  
        !we need a new particle 
        inserted = inserted + 1
        !the first step is assess where to put the particle?
        !1. is there an empty slot in out array?
        if (minval(f(:)%infront)==0) then
          !there is an empty slot in the array
          !now find its location minloc is fortran intrinsic function
          par_new=minloc(f(:)%infront,1)
        else
          !2. we must resize the array - 
          !increment by 1 (speed) - use a dummy array tmp
          allocate(tmp(size(f)+1)) ; tmp(:)%infront=0 !0 the infront array
          !copy accross information
          tmp(1:size(f)) = f
          !now deallocate tmp and transfer particles back to f
          !move_alloc is new intrinsic function (fortran 2003)
          call move_alloc(from=tmp,to=f)
          !pcount+1 must be free - so put the particle there
          par_new=pcount+1 ; pcount=pcount+1 !increase the particle count
        end if
        !insert a new particle between i and infront using curvature
        !get second derivative at i
        call get_deriv_2(i,f_ddot) !general.mod
        curv=sqrt(dot_product(f_ddot,f_ddot)) !get the curvature
        if (curv>1E-5) then !if curvature very small linear interpolation OK
          curv=curv**(-1) !actually we want the inverse
          if (curv**2-0.25*disti**2>0.) then
          !could be negative, avoid this
          f(par_new)%x=0.5*(f(i)%x+f(i)%ghosti)+&
                       (sqrt(curv**2-0.25*disti**2)-curv)*curv*f_ddot
          else
            !linear interpolation
            f(par_new)%x=0.5*(f(i)%x+f(i)%ghosti)
          end if
        else
          !linear interpolation
          f(par_new)%x=0.5*(f(i)%x+f(i)%ghosti)
        end if
        !average the current velocity
        f(par_new)%u=0.5*(f(i)%u+f(f(i)%infront)%u) 
        !set the particles reconnection times
        f(par_new)%t_recon(:)=0.
        !zero the older velocities
        f(par_new)%u1=0. ; f(par_new)%u2=0.
        !set correct infront and behinds & ghostzones
        f(par_new)%behind=i ; f(par_new)%infront=f(i)%infront
        call get_ghost_p(par_new, f(par_new)%ghosti, f(par_new)%ghostb,&
             &           f(par_new)%ghostii, f(par_new)%ghostbb) !periodic.mod

        f(f(i)%infront)%behind=par_new
        call get_ghost_p(f(i)%infront,f(f(i)%infront)%ghosti, f(f(i)%infront)%ghostb, &
             &           f(f(i)%infront)%ghostii, f(f(i)%infront)%ghostbb) !periodic.mod

        f(i)%infront=par_new
        call get_ghost_p(i, f(i)%ghosti, f(i)%ghostb,&
             &              f(i)%ghostii, f(i)%ghostbb) !periodic.mod         
        !set local delta factor to be 1 incase we are adapting it
        f(par_new)%delta=1.
      end if
    end do
    !calculate average separation of particles
    avg_sep=total_length/(old_pcount-count(mask=f(:)%infront==0))

    !make sure that we have separation < delta, one addition may not have been enough
    !this should obviously be done in a more sane way in the main loop, but this is quick and
    !easy and since pinsert is O(n) it shouldn't be too costly
    if(inserted > 0) then
       call pinsert
    end if
  end subroutine

  !>inserts a discretisation point between i and i+1 and near point r
  !>and returns the index of the new vortex point
  function pinsert_near_point(i, r) result(par_new)
    integer, intent(in) :: i
    real, intent(in)    :: r(3);

    integer :: par_new
    real    :: d, wi, dx
    real    :: df(3), ddf(3)
    real    :: i_dot(3), ip1_dot(3) !1st derivatives at i and i+1
    real    :: i_ddot(3), ip1_ddot(3) !2nd derivatives
    real    :: cR, c !curvature radius and curvature

    type(qvort), allocatable, dimension(:) :: tmp

    if(f(i)%infront == 0) call fatal_error('line.mod',&
         & 'trying to insert particle into nothing')

    call get_deriv_1(i, i_dot);
    call get_deriv_1(f(i)%infront, ip1_dot);

    call get_deriv_2(i, i_ddot);
    call get_deriv_2(f(i)%infront, ip1_ddot);

    !find a place for the new point
    if(minval(f(:)%infront) == 0) then !there is a place in the existing array
       par_new = minloc(f(:)%infront, 1);
    else
       !increase the size of the array by 1
       !growing the array this way is slow and should be redone in pinsert as well
       
       allocate(tmp(size(f)+1));
       tmp(1:size(f)) = f;
       call move_alloc(from=tmp, to=f);
       par_new = pcount+1; pcount=pcount+1;
    end if

    !the same sort of algorithm as in pinsert follows, see there for details
    !only we don't place the new point in the middle but such that it is
    !close to point r

    !this is not done exactly by finding the minimum but by simply guessing the
    !"weight" of the i and i%infront points by calculating the projection
    !of r onto (i, i%infront) line segment

    df = f(i)%ghosti - f(i)%x;
    d  = vector_norm(df);
    !the "weight" of point i
    wi = 1 - abs(dot_product(df, r-f(i)%x))/d/vector_norm(r - f(i)%x);

    !sqrts should be better approximation for weighting the second derivative
    !at least this works well for circular arc
    ddf = sqrt(wi)*i_ddot + sqrt(1 - wi)*ip1_ddot;
    c = vector_norm(ddf);
    dx = wi*vector_norm(df);

    if(c > 1e-5 .and. 1/c/c > dx*dx) then
       cR = 1/c;
       ddf = R*ddf;
       f(par_new)%x = wi*f(i)%x + (1-wi)*f(i)%ghosti + (sqrt(cR**2 - dx**2) - cR)*ddf*cR;
    else
       f(par_new)%x = wi*f(i)%x + (1-wi)*f(i)%ghosti
    end if

    if(periodic_bc) then
       call coerce(f(par_new)%x, box_size/2)
    end if

    f(par_new)%u = wi*f(i)%u + (1-wi)*f(f(i)%infront)%u
    f(par_new)%infront = f(i)%infront
    f(par_new)%behind  = i

    f(i)%infront = par_new
    f(f(par_new)%infront)%behind = par_new

    !weighted average of the velocity
    f(par_new)%u = wi*f(i)%u + (1-wi)*f(f(par_new)%infront)%u

    !set the particles reconnection times
    f(par_new)%t_recon(:)=0.
    !zero the older velocities
    f(par_new)%u1=0. ; f(par_new)%u2=0.

    call get_ghost_p(i,f(i)%ghosti, f(i)%ghostb,f(i)%ghostii, f(i)%ghostbb)
    call get_ghost_p(par_new,f(par_new)%ghosti, f(par_new)%ghostb, &
         &           f(par_new)%ghostii, f(par_new)%ghostbb)
    call get_ghost_p(f(par_new)%infront, f(f(par_new)%infront)%ghosti,&
         &           f(f(par_new)%infront)%ghostb, f(f(par_new)%infront)%ghostii, &
         &           f(f(par_new)%infront)%ghostbb)

    !set local delta factor
    f(i)%delta       = dist_gen(f(i)%x, f(i)%ghosti)/delta
    f(par_new)%delta = dist_gen(f(par_new)%x, f(par_new)%ghosti)/delta
  end function pinsert_near_point

  !>inserts a discretisation point between i and i+1 at r and returns
  !>the index of the new vortex point. This function doesn't check
  !>whether that's a sane thing to do, the caller should take care of
  !>that. The velocity of the new point is mean of i, i%infront. Main
  !>purpose of this function is to reconnect the a vortex with a
  !>particle.
  function pinsert_point(i, r) result(par_new)
    integer, intent(in) :: i
    real, intent(in)    :: r(3);

    integer :: par_new

    type(qvort), allocatable, dimension(:) :: tmp

    if(f(i)%infront == 0) call fatal_error('line.mod',&
         & 'trying to insert particle into nothing')

    !find a place for the new point
    if(minval(f(:)%infront) == 0) then !there is a place in the existing array
       par_new = minloc(f(:)%infront, 1);
    else
       !increase the size of the array by 1
       !growing the array this way is slow and should be redone in pinsert as well
       
       allocate(tmp(size(f)+1));
       tmp(1:size(f)) = f;
       call move_alloc(from=tmp, to=f);
       par_new = pcount+1; pcount=pcount+1;
    end if

    !just stick to r, no questions asked
    f(par_new)%x = r
    
    f(par_new)%infront  = f(i)%infront
    f(par_new)%behind   = i

    f(i)%infront  = par_new
    f(f(par_new)%infront)%behind  = par_new

    f(par_new)%u = 0.5*(f(i)%u + f(f(par_new)%infront)%u)

    call get_ghost_p(i,f(i)%ghosti, f(i)%ghostb,f(i)%ghostii, f(i)%ghostbb)
    call get_ghost_p(par_new,f(par_new)%ghosti, f(par_new)%ghostb, &
         &           f(par_new)%ghostii, f(par_new)%ghostbb)
    call get_ghost_p(f(par_new)%infront, f(f(par_new)%infront)%ghosti,&
         &           f(f(par_new)%infront)%ghostb, f(f(par_new)%infront)%ghostii, &
         &           f(f(par_new)%infront)%ghostbb)

    !set local delta factor
    f(i)%delta       = dist_gen(f(i)%x, f(i)%ghosti)/delta
    f(par_new)%delta = dist_gen(f(par_new)%x, f(par_new)%ghosti)/delta
  end function pinsert_point

  !*************************************************************************
  !>remove points along the filament if they are compressed to the point where
  !>the separation between the point i and i+2 is less than \f$\delta\f$
  !>if phonon emission is set to true in run.in then particles with high
  !>curvature are removed, smoothing the loop to mimic dissipation at large k
  subroutine premove
    implicit none
    real :: distii
    integer :: infront, tinfront
    integer :: i, k
    logical :: do_remove, protect
    do i=1, pcount
      if (f(i)%infront==0) cycle !empty particle
      if (mirror_bc) then
        !do not test if you are pinned
        if (f(i)%pinnedi) cycle
        !or the particle infront is pinned
        if (f(f(i)%infront)%pinnedi) cycle
      end if

      do_remove = .false.
      protect   = .false.

      !get the distance between the particle and the one twice infront
      distii=dist_gen(f(i)%x, f(i)%ghostii)
      !if we are simulating phonon emission then call here
      if (phonon_emission) then
        call enforce_phonon_emission(i)
      end if
      if (distii<0.499*delta*(f(i)%delta+f(f(i)%infront)%delta))then
        do_remove=.true.
      end if
      !do not remove points used in tracking reconnection distances
      if (do_remove.and.recon_info) then
         do k=1, n_recon_track
            if (full_recon_distance(k)%active) then
               if (f(i)%infront==full_recon_distance(k)%i) then
                  do_remove = .false.
                  protect   = .true.
               end if
            end if
         end do
      end if
      !do not tamper with trapped particles
      if(f(f(i)%infront)%trapped_particle > 0) do_remove = .false.

      if (do_remove) then
         !print to file the curvature of this particle
         infront=f(i)%infront ; tinfront=f(f(i)%infront)%infront
         !print *, '========removing', i, infront, tinfront, distii
         !print *, '[', f(i)%x, ']'
         !print *, '[', f(i)%ghostii, ']'
         !print *, '[', f(f(f(i)%infront)%infront)%x, ']'

         open(unit=56,file='./data/removed_curv.log',position='append')
         write(56,*) curvature(infront)
         close(56)
         !remove the particle at infront
         f(tinfront)%behind=i ; f(i)%infront=tinfront
         !we must fix the ghosts immediately because they are used to calculate
         !separations
         call get_ghost_p(i, f(i)%ghosti, f(i)%ghostb,&
              &              f(i)%ghostii, f(i)%ghostbb)
         call get_ghost_p(tinfront, f(tinfront)%ghosti, f(tinfront)%ghostb,&
              &                     f(tinfront)%ghostii, f(tinfront)%ghostbb)

         !the ghostii/ghostbb changed for these
         k = f(i)%behind
         call get_ghost_p(k, f(k)%ghosti, f(k)%ghostb,&
              &              f(k)%ghostii, f(k)%ghostbb)
         k = f(tinfront)%infront
         call get_ghost_p(k, f(k)%ghosti, f(k)%ghostb,&
              &              f(k)%ghostii, f(k)%ghostbb)

         call clear_particle(infront) !general.mod
         remove_count=remove_count+1
      end if

      !remove small loops even if they have particles on them
      if(.not.protect) call loop_killer(i)
   end do
 end subroutine premove
!******************************************************************
  !>routine to model phonon emission if the curvature of the segment is
  !>too large then smooth
  subroutine enforce_phonon_emission(i)
    implicit none
    real :: lbefore, lafter
    integer, intent(IN) :: i
    real :: f_ddot(3), curv, disti
    call get_deriv_2(i,f_ddot) !general.mod
    curv=sqrt(dot_product(f_ddot,f_ddot)) !get the curvature
    if (curv>phonon_percent*(2./delta)) then
      !curvature is too large so smooth
      disti=dist_gen(f(i)%ghostb,f(i)%ghosti) !general.f90
      if (recon_info) then
        lbefore=dist_gen(f(i)%ghostb,f(i)%x)+&
                dist_gen(f(i)%x,f(i)%ghosti) 
      end if
      if (curv>1E-5) then !if curvature very small linear interpolation OK
        curv=curv**(-1) !actually we want the inverse
        if (curv**2-0.25*disti**2>0.) then
        !could be negative, avoid this
        f(i)%x=0.5*(f(i)%ghostb+f(i)%ghosti)+&
                     (sqrt(curv**2-0.25*disti**2)-curv)*curv*f_ddot
        else
          !linear interpolation           
          f(i)%x=0.5*(f(i)%ghostb+f(i)%ghosti)
        end if
      else
        !linear interpolation
        f(i)%x=0.5*(f(i)%ghostb+f(i)%ghosti)
      end if
      phonon_count=phonon_count+1 !increment the counter
      if (recon_info) then
        lafter=dist_gen(f(i)%ghostb,f(i)%x)+&
               dist_gen(f(i)%x,f(i)%ghosti) 
        phonon_length=phonon_length+(lbefore-lafter)
      end if
    end if
  end subroutine
  !******************************************************************
  !>find the closest particle to i using N^2 operation this is
  !>done by looping over all particles and caling distf from general.mod
  !\todo we need to do particles on the boundary as well (periodic)
  subroutine pclose
    implicit none
    integer :: i, j
    real :: dist
    !$omp parallel do private(i,j,dist)
    do i=1, pcount
      if (f(i)%infront==0) cycle !empty particle
      f(i)%closestd=100. !arbitrarily high
      if (mirror_bc) then
        !do not test if you are pinned 
        if (f(i)%pinnedi.or.f(i)%pinnedb) cycle 
      end if 
      do j=1, pcount
        if (f(j)%infront==0) cycle !empty particle
        if ((i/=j).and.(f(i)%infront/=j).and.(f(i)%behind/=j)) then
        !the above line ensures we do not reconnect with particles infront/behind
          if (mirror_bc) then
            !do not test if j is pinned 
            if (f(j)%pinnedi.or.f(j)%pinnedb) cycle 
          end if 
          dist=distf(i,j)
          if (dist<f(i)%closestd) then
           f(i)%closest=j
           f(i)%closestd=dist
          end if
        end if
      end do
    end do
    !$omp end parallel do
  end subroutine
  !******************************************************************
  !>reconnect filaments if they become too close - this is a dummy routine which
  !> calls routines in the reconnection modul
  subroutine precon
    implicit none
    select case(recon_type)
      case('original')
        call precon_original !reconnection.mod
      case('schwarz')
        call precon_schwarz !reconnection.mod
      case('dissipative')
        call precon_dissapitive !reconnection.mod
      case('non_dissipative')
        call precon_non_dissipative !reconnection.mod
      case('kondaurova')
        call precon_kondaurova !reconnection.mod
    end select
  end subroutine
end module
