!>general routines e.g. vector products, distance evaluation
!>this is used by all other modules (excluding cdata/derivatives)
module general
  use cdata
  use derivatives
  use statistics
  contains
  !*********************************************************************
  !>empty all a points information - used if a point is removed
  !>(e.g. contraction of filament)
  subroutine clear_particle(i)
    implicit none
    integer, intent(IN) :: i
    f(i)%x=0.
    f(i)%u=0. ; f(i)%u1=0. ; f(i)%u2=0.
    f(i)%ghosti=0. ; f(i)%ghostb=0.
    f(i)%ghostii=0. ; f(i)%ghostbb=0.
    f(i)%infront=0 ; f(i)%behind=0
    f(i)%closest=0 ; f(i)%closestd=0.
    f(i)%pinnedi=.false. ; f(i)%pinnedb=.false.
    f(i)%u_s_LI=0. ; f(i)%u_s_BS=0.
    f(i)%trapped_particle = 0
  end subroutine

  !*********************************************************************
  !>clear tracer particle
  subroutine clear_tracer(i)
    integer, intent(in) :: i;

    p(i)%x = 0.; p(i)%oldx = 0.;
    p(i)%u_new = 0.; p(i)%u = 0.; p(i)%a = 0;

    p(i)%trapping_vparticle = 0;
  end subroutine clear_tracer

  !*********************************************************************
  !>calculate the distance between points in the f vector
  !!The distance between \f$(x_1,y_1,z_1)\f$ and \f$(x_2,y_2,z_2)\f$ is 
  !!\f$\sqrt{(x_2-x_1)^2+(y_2-y_1)^2+(z_2-z_1)^2}\f$.
  real function distf(i,j)
    use Cdata
    implicit none
    integer, intent(IN) :: i, j
    distf=sqrt((f(i)%x(1)-f(j)%x(1))**2+&
               (f(i)%x(2)-f(j)%x(2))**2+&
               (f(i)%x(3)-f(j)%x(3))**2)
  end function
  !*********************************************************************
  !>calculate the squared distance between particles in the f vector
  !!\f${(x_2-x_1)^2+(y_2-y_1)^2+(z_2-z_1)^2}\f$.
  real function distfsq(i,j)
    use Cdata
    implicit none
    integer, intent(IN) :: i, j
    distfsq=(f(i)%x(1)-f(j)%x(1))**2+&
            (f(i)%x(2)-f(j)%x(2))**2+&
            (f(i)%x(3)-f(j)%x(3))**2
  end function
  !*********************************************************************
  !>calculate the curvature at the particle i: \f$|\mathbf{s}''|\f$
  real function curvature(i)
    use Cdata
    implicit none
    integer, intent(IN) :: i
    real :: fddot(3)
    call get_deriv_2(i,fddot)
    curvature=sqrt(dot_product(fddot,fddot))
  end function
  !*********************************************************************
  !>calculate the squared distance between points \f$\mathbf{a}\f$ and \f$\mathbf{b}\f$
  real function dist_gen_sq(a,b)
    use Cdata
    implicit none
    real, dimension(3), intent(IN) :: a, b
    dist_gen_sq=(a(1)-b(1))**2+&
                (a(2)-b(2))**2+&
                (a(3)-b(3))**2
  end function
  !*********************************************************************
  !>calculate the angle between vectors \f$\mathbf{a}\f$ and  \f$\mathbf{b}\f$
  !!\f[ \theta=\cos^{-1} \frac{ \mathbf{a} \cdot \mathbf{b}}{ab} \f]
  real function vector_angle(a,b)
    use Cdata
    implicit none
    real, dimension(3), intent(IN) :: a, b
    vector_angle=acos(dot_product(a,b)/ &
                 (sqrt(dot_product(a,a))*sqrt(dot_product(b,b))))
  end function
  !*********************************************************************
  !>low order finite difference to calculate the tangent vector at point i
  !!\f[
  !!\frac{d \mathbf{s}_i}{d \xi}=\frac{\mathbf{s}_{i+1}-\mathbf{s}_{i}}{\ell_i}
  !! \f]
  function tangentf(i)
    use Cdata
    implicit none
    real, dimension(3) :: tangentf
    integer, intent(IN) :: i
    real :: dist
    !get distance between particle and ghost particle (infront)
    dist=dist_gen(f(i)%x,f(i)%ghosti)
    !now determine the vector (low order finite diff.)
    tangentf(:)=(f(i)%ghosti(:)-f(i)%x(:))/dist
  end function
  !*********************************************************************
  !>calculate the normalised tangent vector at point i low order fininte diff.
  function norm_tanf(i)    
    use Cdata
    implicit none
    real, dimension(3) :: norm_tanf
    integer, intent(IN) :: i
    real :: length !length of vector
    !now determine the vector (low order finite diff.)
    !norm_tanf(:)=(f(i)%ghosti(:)-f(i)%x(:))
    !new method is to use high order finite diff.
    call get_deriv_1(i,norm_tanf)
    !calculate length of vector
    length=sqrt(norm_tanf(1)**2+norm_tanf(2)**2+norm_tanf(3)**2)
    norm_tanf(:)=norm_tanf(:)/length !normalise
  end function
  !*********************************************************************
  !>calculate the normalised normal vector at point i:
  !>\f$\hat{\mathbf{s}''}\f$
  function normalf(i)
    use Cdata
    implicit none
    real, dimension(3) :: normalf
    integer, intent(IN) :: i
    real :: length !length of vector
    !get the second derivative
    call get_deriv_2(i,normalf)
    !calculate length of vector
    length=sqrt(normalf(1)**2+normalf(2)**2+normalf(3)**2)
    normalf(:)=normalf(:)/length !normalise
  end function
  !*********************************************************************
  !>calculate the normalised binormal vector at point i:
  !>\f$\hat{\mathbf{s}'} \times \hat{\mathbf{s}''}\f$
  function binormalf(i)
    use Cdata
    implicit none
    real, dimension(3) :: s_dot, s_ddot, binormalf
    integer, intent(IN) :: i
    real :: length !length of vector
    !get the first/second derivative
    call get_deriv_1(i,s_dot)
    call get_deriv_2(i,s_ddot)
    binormalf=cross_product(s_dot,s_ddot)
    !calculate length of vector
    length=sqrt(binormalf(1)**2+binormalf(2)**2+binormalf(3)**2)
    binormalf(:)=binormalf(:)/length !normalise
  end function
  !*********************************************************************
  !>calculate the cross product of \f$\mathbf{a}\f$ and \f$\mathbf{b}\f$
  function cross_product(a,b)
    implicit none
    real, dimension(3) :: cross_product
    real, dimension(3), intent(IN) :: a, b
    cross_product(1)=a(2)*b(3)-a(3)*b(2)
    cross_product(2)=a(3)*b(1)-a(1)*b(3)
    cross_product(3)=a(1)*b(2)-a(2)*b(1)
  end function
  !*********************************************************************
  !>calculate the L2-norm of input a
  !!\f[ |mathbf{x}|=\sqrt{x_1^2+x_2^2+x_3^2} \f]
  real function vector_norm(a)
    use Cdata
    implicit none
    real, dimension(3), intent(IN) :: a
    vector_norm=(sqrt(dot_product(a,a)))
  end function
  !*********************************************************************
  !>calculate sech
  !!\f[ \sech(x)= 2./(exp(x)+exp(-x)) \f]
  real function sech(a)
    use Cdata
    implicit none
    real, intent(IN) :: a
    sech=2./(exp(a)+exp(-a))
  end function
  !**************************************************
  !>a routine to test if two points are on the same loop
  !>returns a logical arguement with the answer
  subroutine same_loop_test(i,j,same_loop)
     use Cdata
     implicit none
     integer,intent(IN) :: i,j
     integer :: k
     integer :: next
     logical :: same_loop
     !aim  of routine is to find out wether the links are on the same loop
     same_loop=.false. !initial condition now try and prove if true
     next=i
     do k=1, pcount
       next=f(next)%infront
       if (next==j) then
         same_loop=.true.
         exit
       end if
       if (next==i) exit
     end do
   end subroutine
  !**************************************************
  !>a routine to test if two points are on the same loop
  !>returns a logical arguement with the answer
  !>this routine also returns the separation of the points
  !>along the filament
  subroutine same_loop_test_arc_length(i,j,same_loop,arc_dist)
     use Cdata
     implicit none
     integer,intent(IN) :: i,j !the points
     integer,intent(OUT) :: arc_dist !what is the separation between i&j
     integer :: k !for looping
     integer :: next !helper
     logical :: same_loop !are the points on the same loop?
     integer :: arc_dist_dummy !helper
     !aim  of routine is to find out wether the links are on the same loop
     same_loop=.false. !initial condition now try and prove if true
     !initialise
     next=i
     arc_dist_dummy=0
     do k=1, pcount
       next=f(next)%infront
       arc_dist_dummy=arc_dist_dummy+1
       if (next==j) then
         same_loop=.true.
         exit
       end if
       if (next==i) exit
     end do
     arc_dist=arc_dist_dummy
     !now we go the other way and see if arc_dist is reduced 
     if (same_loop) then !only do if on same loop!
       !re-initialise
       next=i
       arc_dist_dummy=0
       do k=1, pcount
         next=f(next)%behind
         arc_dist_dummy=arc_dist_dummy+1
         if (next==j) then
           exit
         end if
         !probably don't need this but keep in for safety
         if (next==i) then
           call fatal_error('same_loop_test_arc_length','major issue')
         end if
       end do
     end if
     !if arc distance has been reduced then set to new value
     if (arc_dist_dummy<arc_dist) then
       arc_dist=arc_dist_dummy
     end if
   end subroutine
  !*********************************************************************
  !> a routine to find any NANs in the positions of all the allocated main
  !> arrays
  subroutine NAN_finder
    implicit none
    integer :: i
    if (allocated(f)) then
      if (any(isnan(f%x(1))).or.any(isnan(f%x(2))).or.any(isnan(f%x(3)))) then
        do i=1, pcount
           !check positions, velocity and ghosts

          if ((isnan(f(i)%x(1))).or. &
              (isnan(f(i)%x(2))).or. &
              (isnan(f(i)%x(3)))) then
            write(*,*) 'I have found a NAN in the f%x array'
            write(*,*) 'location, i=',i
            write(*,*) 'f(i)%x=',f(i)%x
          end if

          if ((isnan(f(i)%u(1))).or. &
              (isnan(f(i)%u(2))).or. &
              (isnan(f(i)%u(3)))) then
            write(*,*) 'I have found a NAN in the f%u array'
            write(*,*) 'location, i=',i
            write(*,*) 'f(i)%u=',f(i)%u
          end if

          if ((isnan(f(i)%ghosti(1))).or. &
              (isnan(f(i)%ghosti(2))).or. &
              (isnan(f(i)%ghosti(3)))) then
            write(*,*) 'I have found a NAN in the f%ghosti array'
            write(*,*) 'location, i=',i
            write(*,*) 'f(i)%ghosti=',f(i)%ghosti
         end if

        end do
        call fatal_error('run.x','there is a NAN in the (filament) f%x array')
      end if
    end if
    if (allocated(g)) then
      if (any(isnan(g%x(1))).or.any(isnan(g%x(2))).or.any(isnan(g%x(3)))) then
        call fatal_error('run.x','there is a NAN in the (quasi particle) g%x array')
      end if
    end if
    if (allocated(p)) then
      if (any(isnan(p%x(1))).or.any(isnan(p%x(2))).or.any(isnan(p%x(3)))) then
        call fatal_error('run.x','there is a NAN in the (particle) p%x array')
      end if
    end if
  end subroutine

  subroutine check_line_integrity(delta_tolerance)
    integer :: i, j
    logical :: checked(pcount)

    real, optional :: delta_tolerance
    real :: dtol = 0, cif(3)

    if(present(delta_tolerance)) then
       dtol = delta_tolerance
    end if

    checked = .false.

    do i=1, pcount
       if(checked(i)) cycle

       if(f(i)%infront /= 0) then
          j = i
          do
             if(j == i) exit

             if(f(j)%infront == 0) then
                print *, 'open line between', i, j
                call fatal_error('lines', 'open line')
             end if

             if(j /= f(f(j)%infront)%behind .or.&
              & j /= f(f(j)%behind)%infront) then
                print *, 'point', j, 'inconsistent'
                call fatal_error('lines', 'corrupted f-array')
             end if

             checked(j) = .true.
             j = f(j)%infront
          end do

          cif = f(f(i)%infront)%x
          call coerce2(f(i)%x, cif, box_size/2);

          if(dist_gen(f(i)%x, f(i)%ghosti) > delta*(1 + dtol)) then
             print *, '------------------'
             print *, 'line under-meshed', f(i)%behind, i, f(i)%infront,&
                  & dist_gen(f(i)%x, f(i)%ghosti), delta*(1+dtol)
             print *, 'deltas', f(i)%delta, f(f(i)%infront)%delta
             print *, '[', f(i)%x, '] [', f(i)%ghosti, '] [', cif, ']'
             call print_particle(f(i)%trapped_particle)
             call print_particle(f(f(i)%infront)%trapped_particle)
             call print_particle(f(f(i)%behind)%trapped_particle)
             call fatal_error('lnes', 'not enough mesh points');
          end if
       end if
       
    end do
  end subroutine check_line_integrity

  subroutine check_particle_integrity
    integer :: i

    do i=1, part_count
       if(p(i)%trapping_vparticle > 0) then
          
          !check the trapping book keeping
          if(f(p(i)%trapping_vparticle)%trapped_particle /= i) then
             print *, 'particle', i, 'trapped on', p(i)%trapping_vparticle
             print *, 'vortex reports', f(p(i)%trapping_vparticle)%trapped_particle

             call fatal_error('particles', 'corrupted particles')
          end if

          !check that we aren't removing vortex point under particles in remeshing
          if(f(p(i)%trapping_vparticle)%infront == 0) then
             print *, 'particle', i, 'trapped on empty point', p(i)%trapping_vparticle

             call fatal_error('particles', 'corrupted particles')
          end if

          !check that the particle and vortex point positions match
          if(.not.real_eqv(0., vector_norm(p(i)%x - f(p(i)%trapping_vparticle)%x))) then
             print *, 'particle', i, 'at', p(i)%x
             print *, 'the vortex at', f(p(i)%trapping_vparticle)%x
             print *, 'dist:', vector_norm(p(i)%x - f(p(i)%trapping_vparticle)%x)

             call fatal_error('particles', 'corrupted particles')
          end if
       end if
    end do
  end subroutine check_particle_integrity
    
  !*********************************************************************
  !> a routine purely used for code testing finds empty particles 
  subroutine zero_finder(location)
    implicit none
    integer :: i, zcount=0
    character(len=*) :: location
    do i=1, pcount
      if (f(i)%infront==0) zcount=zcount+1
    end do
    write(*,*) 'zero finder called at', trim(location)
    write(*,*) 'zero count= ', zcount
    write(*,*) 'ending run...' ; stop 
  end subroutine

  !*********************************************************************
  !> superfluid velocity at point x induced by segment (a,b) 
  function segment_field(a, b, x) result(du)
    real, intent(in) :: a(3), b(3), x(3)
    real             :: du(3)
    real             :: a_bs, b_bs, c_bs(3)
    a_bs = vector_norm(a - x) !lRj
    b_bs = vector_norm(b - x) !lRjp1

    !denom
    c_bs = a_bs*b_bs*(a_bs*b_bs + dot_product(a - x,b - x))

    du = (a_bs + b_bs)/c_bs*cross_product(a - x, b - x)*quant_circ/4/pi
  end function segment_field

  !returs k-th cartesian basis vector
  function cek(k)
    real :: cek(3)
    integer, intent(in) :: k

    cek = 0;
    cek(k) = 1;
  end function cek

  !equivalence of reals
  function real_eqv(a, b)
    real    :: a, b;
    logical :: real_eqv

    real_eqv = (abs(a-b) < epsilon(a))
  end function real_eqv

  !calculates the tangent at point f(i) from the right
  !interpolates 3 points i, i+1, i+2 with a parabola and calculates the
  !derivative at point i. The parametrization of the parabola is by
  !segment lengths, i.e. s(z) being the parabola,
  !s(0) = f(i), s(l1) = f(i+1), s(l1+l2) = f(i+2)
  !(f(i+1) should be understood as f(f(i)%infront))
  function right_tangentf(i) result(sp_r)
    integer :: i
    real :: l1, l2
    real :: x0(3), x1(3), x2(3)
    real :: sp_r(3)

    x0  = f(i)%x
    x1  = f(f(i)%infront)%x
    x2  = f(f(f(i)%infront)%infront)%x
    
    if (periodic_bc) then
       !we must ensure that x1 / x2 is not on the other side of the box
       call coerce2(x0, x1,  box_size/2)
       call coerce2(x0, x2,  box_size/2)
    else if (periodic_bc_notx) then
       call coerce2(x0, x1,  box_size/2, [2,3])
       call coerce2(x0, x2,  box_size/2, [2,3])
    else if (periodic_bc_notxy) then
       call coerce2(x0, x1,  box_size/2,  [3])
       call coerce2(x0, x2,  box_size/2,  [3])
    end if

    l1  = dist_gen(x0, x1)
    l2  = dist_gen(x1, x2)

    if(l1 > box_size/2 .or. l2 > box_size/2) then
       print *, x0, x1, x2
       call fatal_error('general::right_tangentf', 'coercrion didnt work')
    end if

    sp_r = -(l1**2*x2 - (l1+l2)**2*x1 + l2*(l2 + 2*l1)*x0)/&
         &  (l1 + l2)/l1/l2

    sp_r = sp_r / vector_norm(sp_r)
  end function right_tangentf

  !similar as above except now the parabola goes through
  !i-2 (x2), i-1 (x1), i(x0) and the derivative is evaluated
  !and the derivative is taken with a negative sign because we
  !reversed the parametrization; otherwise the code is identical
  !to left_tangentf
  function left_tangentf(i) result(sp_l)
    integer :: i
    real :: l1, l2
    real :: x0(3), x1(3), x2(3)
    real :: sp_l(3)

    x0 = f(i)%x
    x1 = f(f(i)%behind)%x
    x2 = f(f(f(i)%behind)%behind)%x

    if (periodic_bc) then
       !we must ensure that x1 / x2 is not on the other side of the box
       call coerce2(x0, x1, box_size/2)
       call coerce2(x0, x2, box_size/2)
    else if (periodic_bc_notx) then
       call coerce2(x0, x1, box_size/2, [2,3])
       call coerce2(x0, x2, box_size/2, [2,3])
    else if (periodic_bc_notxy) then
       call coerce2(x0, x1, box_size/2, [3])
       call coerce2(x0, x2, box_size/2, [3])
    end if

    l1 = dist_gen(x0, x1)
    l2 = dist_gen(x1, x2)

    if(l1 > box_size/2 .or. l2 > box_size/2) then
       print *, x0, x1, x2
       call fatal_error('general::left_tangentf', 'coercrion didnt work')
    end if

    sp_l = -(l1**2*x2 - (l1+l2)**2*x1 + l2*(l2 + 2*l1)*x0)/&
         &  (l1 + l2)/l1/l2
    
    !negative sign because of the reversed parameterization
    sp_l = -sp_l / vector_norm(sp_l)
  end function left_tangentf  

  !>coerces value val to (-limit, limit)
  subroutine coerce_scalar(val, limit)
    real, intent(inout) :: val
    real, intent(in)    :: limit
    do
       if(val < limit) exit
       val = val - limit*2
    end do
    do
       if(val > -limit) exit
       val = val + limit*2
    end do
  end subroutine coerce_scalar

  !>coerces b so that (a - b) \in (-limit, limit)
  subroutine coerce2_scalar(a, b, limit)
    real, intent(inout) :: b
    real, intent(in)    :: a, limit
    do
       if(a - b < limit) exit
       b = b + limit*2
    end do
    do
       if(a - b > -limit) exit
       b = b - limit*2
    end do
  end subroutine coerce2_scalar

  !>coerces all elements of a vector
  subroutine coerce(vec, limit, dims)
    real, intent(inout) :: vec(3)
    real, intent(in)    :: limit

    integer, intent(in), dimension(:), optional :: dims

    integer :: k
    if(present(dims)) then 
       do k=1,size(dims)
          call coerce_scalar(vec(dims(k)), limit)
       end do
    else
       do k=1,3
          call coerce_scalar(vec(k), limit)
       end do
    end if
  end subroutine coerce
  
  subroutine coerce2(a, b, limit, dims)
    real, intent(inout) :: b(3)
    real, intent(in)    :: a(3), limit

    integer, intent(in), dimension(:), optional :: dims

    integer :: k
    if(present(dims)) then 
       do k=1,size(dims)
          call coerce2_scalar(a(dims(k)), b(dims(k)), limit)
       end do
    else
       do k=1,3
          call coerce2_scalar(a(k), b(k), limit)
       end do
    end if
  end subroutine coerce2
  
end module
