!>The particles aspect of the code. At present this evolves either
!>fluid or inertial particles in the code. The type of particle is set
!>by the particle_type parameter in run.in Fluid/Inertial/tracer
!>particles use the normal fluid velocity. The initial setup of the
!>particles is set throught the initp parameter. See \ref PARTICLES
!>for more details
module particles
  use cdata
  use general
  use timestep
  use output
  use line
  real :: part_maxu=0., part_maxdu=0., part_urms=0. !velocity information
  real :: part_sep=0. !average particle separation
  contains
  !************************************************************
  !>setup the particles in as set by initp
  subroutine setup_particles
    implicit none
    integer :: i, k
    integer :: counter
    real :: rand1, rand2, rand3, sphere_theta, sphere_phi
    allocate(p(part_count))
    write(*,*) 'setting up particles in ', trim(initp),' configuration'
    write(*,'(a,a,a,i4.1)') ' particle type: ', trim(particle_type), ', particle count: ', part_count
    select case(initp)
      !**************************FLUID/INERITAL-PARTICLES*************************
      !put in all the fluid/inertial particle initial conditions in the code here
      case('one-side')
        !particles all on one side of the box (x-axis) 
        do i=1, part_count
          p(i)%x(1)=-box_size/2.
          call random_number(p(i)%x(2)) ; call random_number(p(i)%x(3))
          p(i)%x(2)=box_size*p(i)%x(2)-box_size/2.
          p(i)%x(3)=box_size*p(i)%x(3)-box_size/2.
          p(i)%u=0. ; p(i)%u1=0. ; p(i)%u2=0. !0 the velocity fields
        end do
      case('random')
        !particles in random positions
        do i=1, part_count
          call random_number(p(i)%x(1))
          call random_number(p(i)%x(2)) ; call random_number(p(i)%x(3))
          if (box_size>0.) then
            p(i)%x(1)=box_size*p(i)%x(1)-box_size/2.
            p(i)%x(2)=box_size*p(i)%x(2)-box_size/2.
            p(i)%x(3)=box_size*p(i)%x(3)-box_size/2.
          end if
          p(i)%u=0. ; p(i)%u1=0. ; p(i)%u2=0. !0 the velocity fields
        end do
      case('ring')
        !particles in random positions
        do i=1, part_count
          p(i)%x(1)=0.75*box_size/2.*cos(2*pi*(2.*i-1.)/(2.*part_count))
          p(i)%x(2)=0.75*box_size/2.*sin(2*pi*(2.*i-1.)/(2.*part_count))
          !p(i)%x(3)=-box_size/2.
          p(i)%x(3)=0.
          p(i)%u=0. ; p(i)%u1=0. ; p(i)%u2=0. !0 the velocity fields
        end do
      case('tube')
        write(*,'(a,f6.4,a)') ' tube radius: ', part_tube_ratio, ' of box size'
        !particles in random positions
        do i=1, part_count
          rand1=runif(0.,box_size*part_tube_ratio)
          rand2=runif(0.,2.*pi)
          rand3=runif(-box_size/2.,box_size/2.)
          p(i)%x(1)=rand1*cos(rand2)
          p(i)%x(2)=rand1*sin(rand2)
          p(i)%x(3)=rand3
          p(i)%u=0. ; p(i)%u1=0. ; p(i)%u2=0. !0 the velocity/Users/abaggaley/Work/code/qvort fields
        end do
      case('crow_tube')
        write(*,'(a,f6.4,a)') ' tube radius: ', part_tube_ratio, ' of box size'
        do i=1, part_count/2
          rand1=runif(0.,box_size*part_tube_ratio)
          rand2=runif(0.,2.*pi)
          p(i)%x(1)=rand1*cos(rand2)
          p(i)%x(3)=-box_size/2.+box_size*real(2*i-1)/(part_count)
          p(i)%x(2)=delta-(delta/16.)*sin(pi*(box_size/2.+p(i)%x(3))/box_size)+rand1*sin(rand2)
          p(i)%u=0. ; p(i)%u1=0. ; p(i)%u2=0. !0 the velocity fields
        end do
        do i=part_count/2+1, part_count
          rand1=runif(0.,box_size*part_tube_ratio)
          rand2=runif(0.,2.*pi)
          p(i)%x(1)=rand1*cos(rand2)
          p(i)%x(3)=box_size/2.-box_size*real(2*(i-part_count/2)-1)/(part_count)
          p(i)%x(2)=-delta+(delta/16.)*sin(pi*(box_size/2.-p(i)%x(3))/box_size)+rand1*sin(rand2)
          p(i)%u=0. ; p(i)%u1=0. ; p(i)%u2=0. !0 the velocity fields
        end do
      case('orthog_tube')
        write(*,'(a,f6.4,a)') ' tube radius: ', part_tube_ratio, ' of box size'
        do i=1, part_count/2
          rand1=runif(0.,box_size*part_tube_ratio)
          rand2=runif(0.,2.*pi)
          p(i)%x(1)=rand1*cos(rand2)
          p(i)%x(3)=-box_size/2.+box_size*real(2*i-1)/(part_count)
          p(i)%x(2)=0.5*delta+rand1*sin(rand2)
          p(i)%u=0. ; p(i)%u1=0. ; p(i)%u2=0. !0 the velocity fields
        end do
        do i=part_count/2+1, part_count
          rand1=runif(0.,box_size*part_tube_ratio)
          rand2=runif(0.,2.*pi)
          p(i)%x(1)=box_size/2.-box_size*real(2*(i-pcount/2)-1)/(part_count)
          p(i)%x(2)=-0.5*delta+rand1*sin(rand2)
          p(i)%x(3)=rand1*cos(rand2)
          p(i)%u=0. ; p(i)%u1=0. ; p(i)%u2=0. !0 the velocity fields
        end do
      case('central_sphere')
        write(*,'(a,f6.4,a)') ' sphere radius: ', part_sphere_radius, ' of box size'
        do i=1, part_count
          rand1=runif(0.,part_sphere_radius*box_size) !r - distance from centre
          rand2=runif(0.,1.)
          rand3=runif(0.,1.)
          sphere_theta=2.*pi*rand2
          sphere_phi=acos(2.*rand3-1.)
          p(i)%x(1)=rand1*cos(sphere_theta)*sin(sphere_phi)
          p(i)%x(2)=rand1*sin(sphere_theta)*sin(sphere_phi)
          p(i)%x(3)=rand1*cos(sphere_phi)
          p(i)%u=0. ; p(i)%u1=0. ; p(i)%u2=0. !0 the velocity fields
        end do
      case('pairs')
        !particles in pairs in a random position
        !check that the number of particles is a multiple of 2
        if (mod(part_count,2)/=0) then
          call fatal_error('setup_particles','part_count must be a multiple of 2')
        end if
        do i=1,  part_count, 2
          call random_number(p(i)%x(1))
          call random_number(p(i)%x(2)) ; call random_number(p(i)%x(3))
          p(i)%x(1)=box_size*p(i)%x(1)-box_size/2.
          p(i)%x(2)=box_size*p(i)%x(2)-box_size/2.
          p(i)%x(3)=box_size*p(i)%x(3)-box_size/2.
          p(i)%u=0. ; p(i)%u1=0. ; p(i)%u2=0. !0 the velocity fields
          !finally sort out the position of its neighbour
          p(i+1)%x=p(i)%x ; p(i+1)%x(3)=p(i)%x(3)+0.001
          p(i+1)%u=0. ; p(i+1)%u1=0. ; p(i+1)%u2=0. !0 the velocity fields
        end do
      case('lattice')
        counter=0
        do i=1, floor(sqrt(real(part_count))) ; do k=1, floor(sqrt(real(part_count)))
          p((i-1)*floor(sqrt(real(part_count)))+k)%x(1)=(-box_size/2.+box_size*((2.*i-1.)/(2*sqrt(real(part_count)))))*lattice_ratio
          p((i-1)*floor(sqrt(real(part_count)))+k)%x(2)=(-box_size/2.+box_size*((2.*k-1.)/(2*sqrt(real(part_count)))))*lattice_ratio
          p((i-1)*floor(sqrt(real(part_count)))+k)%x(3)=0.
          counter=counter+1
        end do ; end do
        if (counter/=part_count) then
          call fatal_error('init.mod:setup_particles', &
          'part_count must be a square number')  
        end if
      case ('single_vortex_collision')
         if(.not. (initf .eq. 'single_line')) then
            call fatal_error('init::steup_particles', 'this particle setup meant for&
                 &single_line finit');
         end if

         do i=1, part_count
            call clear_tracer(i);
            p(i)%x(1) = initpx_single_col
            p(i)%x(3) = -box_size/2 + i*box_size/(part_count + 1)
            select case(initp_vel)
               case('normal_fluid')
                  call get_normal_velocity(p(i)%x, p(i)%u)
               case ('zero')
                  p(i)%u = 0.
               case ('set_val')
                  p(i)%u = initp_vel_val
               case default
                  print *, initp_vel
                  call fatal_error('setup_particles', 'unknown initp_vel')
            end select
         end do
      case default
        print *, initp
        call fatal_error('setup_particles','initp not set to available value')
    end select
    !check the particles type
    select case(particle_type)
      case('fluid')
        !do nothing
      case('inertial')
        if (part_stokes<epsilon(0.)) call fatal_error('setup_particles',&
        'part_stokes is 0, change particle_type if you want tracers')
        write(*,'(a,f10.4)') 'stokes number is ', part_stokes
     case('tracer')
        !check that the (optional) stokes drag is set up properly
        !tracer_stokes_tau =0 by default
        if (tracer_stokes .and. tracer_stokes_tau<epsilon(0.)) then
           call fatal_error('setup_particles',&
                'tracer_stokes_tau must be >0 if stokes drag is on!')
        end if
        if(tracer_stokes) then
           print *, 'stokes tau is:', tracer_stokes_tau
        else
           write(*,'(a)') ' stokes drag is OFF'
        end if
        
        write(*,'(a,f10.8)') ' trapping distance/cutoff is ', tracer_trap_d
        if(tracer_trapping) write(*, *) 'will trap particles on nearby vortices'
        if(trapped_drag) write(*, *) 'trapped particles will feel drag and inertial forces'
        print *, 'tracer resolution warning threshold', warn_resolution
      case default
        call fatal_error('setup_particles','particle type incorrect')
    end select
  end subroutine
  !************************************************************
  !>evolve the particles in the code and print to file/perform 
  !>diagnostic tests
  subroutine particles_evolution
    implicit none
    !timestep the particles
    call timestep_particles
    !particle diagnostics
    if (mod(itime,shots)==0) then
      call diagnostics_particles
    end if
    !print the particles to file
    if (mod(itime,shots)==0) then
          call printt(itime/shots)
          call printp(itime/shots) !output.mod
    end if
    !any other business in here
  end subroutine
  !***********************************************************
  !>timestep the particles
  subroutine timestep_particles
    implicit none
    !particle velocity, normal fluid velocity and the step
    real :: u(3), vn(3), dx(3)
    !one-sided tangents for vortex tension
    real :: sp_left(3), sp_right(3), sp(3)
    !vortex tension
    real :: T0, lp, lm
    integer :: i
    !$omp parallel do private(i, u, vn, dx, sp_left, sp_right, T0, lp, lm, sp)
    do i=1, part_count
       !store the old accelerations before we start calculating new ones
       p(i)%a3 = p(i)%a2; p(i)%a2 = p(i)%a1; p(i)%a1 = p(i)%a;
       p(i)%a = 0
      select case(particle_type)
        case('fluid')
          call velocity_fluidp(i,u)
          p(i)%u=u
        case('inertial')
          call velocity_fluidp(i,u)
          !an euler step to get the velocity
          p(i)%u=p(i)%u1+dt*(u-p(i)%u1)*part_stokes
          !adjust the velocity due to stokes drag
       case('tracer')
          !only do this for free particles or when trapped particles
          !are dragged
          if(trapped_drag .or. p(i)%trapping_vparticle == 0) then
             !0 the accelreation parts and start adding the contributions
             p(i)%dtvs = 0; p(i)%vs_g_vs = 0; p(i)%vs = 0; p(i)%a = 0

             call get_normal_velocity(p(i)%x, vn) !normal_fluid.mod
             
             !stokes drag, tracer_stokes can be set through run.in, true by default
             p(i)%a_stokes = -(p(i)%u - vn)/tracer_stokes_tau
             if(tracer_stokes) then
                p(i)%a = p(i)%a + p(i)%a_stokes
             end if

             !inertial forces, tracer_inertial can be set through run.in, true by default
             if(tracer_inertial) then
                !these accelrations are calculated only if requested because they are expensive

                !Careful! Because of the non-linear term, periodicity doesn't mean
                !simple summing over shifts. This is handled internally in the *_mat_diff functions
                !A slight problem: these calls are calculating the derivatives only AFTER the vortices
                !already moved
                select case(velocity)
                  case('BS')
                    call bs_mat_diff(p(i)%x, p(i)%dtvs, p(i)%vs_g_vs, p(i)%vs& !timestep
                       &          ,cutoff   = tracer_trap_d&
                       &          ,periodic = periodic_bc&
                       !for free particles trapping_vparticle == 0 and the code will work as if
                       !oskip wasn't supplied
                       &          ,oskip    = p(i)%trapping_vparticle)
                  case('Tree')
                     call tree_mat_diff(vtree, p(i)%x, p(i)%dtvs, p(i)%vs_g_vs, p(i)%vs& !timestep
                       &          ,cutoff   = tracer_trap_d&
                       &          ,periodic = periodic_bc&
                       &          ,oskip    = p(i)%trapping_vparticle)
                  case default
                    call fatal_error('particles.mod', &
                         &'Only BS or Tree is supported for tracer particles\n.');
                end select

                if(p(i)%trapping_vparticle > 0) then
                   if(f(p(i)%trapping_vparticle)%infront == 0) then
                      call fatal_error('particles', 'trapped on an empty vortex particle!')
                   end if
                   !left and right tangents
                   sp_left  = left_tangentf(p(i)%trapping_vparticle)
                   sp_right = right_tangentf(p(i)%trapping_vparticle)

                   lp = vector_norm(f(p(i)%trapping_vparticle)%x -&
                        &     f(p(i)%trapping_vparticle)%ghosti)
                   lm = vector_norm(f(p(i)%trapping_vparticle)%x -&
                        &     f(p(i)%trapping_vparticle)%ghostb)

                   !vortex tension
                   T0 = rho_s*quant_circ**2/4/pi*log(2*sqrt(lp*lm)/sqrt(e)/corea)

                   !Mineda et.al., PRB 87, 174508 (2013)

                   !tension

                   !tangent at the particle
                   call get_deriv_1(p(i)%trapping_vparticle, sp)
                   sp = sp / vector_norm(sp)
                   
                   !we only want the component perpendicular to sp -- tension
                   !shouldn't move the point along the line
                   p(i)%a_tension = T0*((sp_right - sp_left) &
                        &- sp*dot_product(sp, sp_right - sp_left))/ tracer_eff_mass                 
                   
                   !Magnus force
                   p(i)%a_magnus  = rho_s*quant_circ*&
                        &cross_product(sp, p(i)%u - p(i)%vs) / tracer_eff_mass
                   p(i)%a_magnus = p(i)%a_magnus*0.5*(lp + lm)

                   !mutual friction
                   p(i)%a_MF = gamma_0(1)*&
                        &cross_product(sp, cross_product(sp, p(i)%u - vn))
                   p(i)%a_MF = p(i)%a_MF + gamma_0(2)*cross_product(sp, p(i)%u - vn)
                   p(i)%a_MF = p(i)%a_MF*0.5*(lp + lm) / tracer_eff_mass

                   p(i)%a = p(i)%a + p(i)%a_tension&
                        &          + p(i)%a_magnus&
                        &          + p(i)%a_MF

                end if

                !above only the literal dtvs or vs_g_vs is calculated, the
                !actual acceleration is the force divided by the effective mass
                !rho_s defaults to 0 so BY DEFAULT THE PARTICLE WON'T FEEL THE
                !INERTIAL FORCE, rho_s must be set explicitly. For now assuming
                !tracer_rho == rho_He

                p(i)%a = p(i)%a + 1.5*rho_s*tracer_volume*(p(i)%dtvs + p(i)%vs_g_vs)/tracer_eff_mass
             end if

             !adams-bashforth-step the particles' velocity, if possible
             !otherwise, euler
             if (maxval(abs(p(i)%a3)) > 0) then !4th order
                p(i)%u_new = p(i)%u + dt*(55./24.*p(i)%a - 59./24.*p(i)%a1 + 37./24.*p(i)%a2&
                           &             -3./8.*p(i)%a3)
             else if(maxval(abs(p(i)%a2)) > 0) then !3rd order
                p(i)%u_new = p(i)%u + dt*(23./12.*p(i)%a - 4./3.*p(i)%a1 +5./12.*p(i)%a2)
             else if(maxval(abs(p(i)%a1)) > 0) then !2nd order
                p(i)%u_new = p(i)%u + dt*(1.5*p(i)%a - 0.5*p(i)%a1)
             else !euler
                p(i)%u_new = p(i)%u + dt*p(i)%a
             end if

          else !particle is trapped, it's velocity is that of its trapping vortex point
             p(i)%u = f(p(i)%trapping_vparticle)%u
             p(i)%vs = f(p(i)%trapping_vparticle)%u_sup
          end if
       end select

       if(p(i)%trapping_vparticle > 0) then
          !if it's trapped, consider the movement 'exact'
          p(i)%resolution = 0;
       end if

       if(.not.trapped_drag .and. p(i)%trapping_vparticle > 0) then
          !if trapped, just go wherever the vortex went
          p(i)%oldx = p(i)%x
          p(i)%x = f(p(i)%trapping_vparticle)%x
       else !for free (or trappend with trapped_drag) particle,
          !move with velocity p(i)%u (and history)
          if(maxval(abs(p(i)%u3)) > 0) then
             dx = dt*(55./24.*p(i)%u - 59./24.*p(i)%u1 + 37./24.*p(i)%u2&
                  &         -3./8.*p(i)%u3)
          else if(maxval(abs(p(i)%u2)) > 0) then !3rd order AB
             dx = dt*(23./12.*p(i)%u - 4./3.*p(i)%u1 +5./12.*p(i)%u2)
          else if(maxval(abs(p(i)%u1)) > 0) then !2nd order AB
             dx = dt*(1.5*p(i)%u - 0.5*p(i)%u1)
          else !euler
             dx = dt*p(i)%u
          end if
          
          p(i)%resolution = 0
          if(p(i)%trapping_vparticle == 0) then
             p(i)%resolution = vector_norm(dx)/p(i)%nearest_vp
             if(p(i)%resolution > warn_resolution) then
                print *, 'Warning: particle', i, 'under-resolved (resolution: ',&
                     p(i)%resolution, ').'
             end if
          end if
          p(i)%oldx = p(i)%x
          p(i)%x = p(i)%x + dx
          p(i)%u = p(i)%u_new
       end if
       
       !update the old velocities regardless the particle being trapped or not
       p(i)%u3 = p(i)%u2; p(i)%u2 = p(i)%u1; p(i)%u1 = p(i)%u;
       if(p(i)%trapping_vparticle == 0) p(i)%u = p(i)%u_new !free particle
       
       !enforce periodicity
       if (periodic_bc) call coerce(p(i)%x, box_size/2)
    end do
    !$omp end parallel do

    !if the trapped particles drag the vortex, update all in one place to avoid
    !possible data races
    if(trapped_drag) then
       !$omp parallel do private(i)
       do i=1, part_count
          if(p(i)%trapping_vparticle > 0) then !in this case the vortex point moves with the particle
             f(p(i)%trapping_vparticle)%x = p(i)%x
             f(p(i)%trapping_vparticle)%u = p(i)%u
          endif
       end do
       !$omp end parallel do
    end if
  end subroutine
  !************************************************************
  !> calculate the velocity at each particle
  !> note we can set this in here to be the velocity induced by
  !> the superfluid vortices but this is unphysical
  !> really this should be false by default
  subroutine velocity_fluidp(i,u)
    implicit none 
    integer, intent(IN) :: i
    real, intent(OUT) :: u(3)
    real :: u_sup(3), u_norm(3)
    integer :: peri, perj, perk
    u_sup=0. !must be zeroed for intially
    if (particle_super_velocity) then
      select case(velocity)
        case('LIA','BS')
          call biot_savart_general(p(i)%x,u_sup) !timestep.mod
        case('Tree')
          call tree_walk_general(p(i)%x,vtree,(/0.,0.,0./),u_sup)
          if (periodic_bc) then
            !we must shift the mesh in all 3 directions, all 26 permutations needed!
            do peri=-1,1 ; do perj=-1,1 ; do perk=-1,1
              if (peri==0.and.perj==0.and.perk==0) cycle
              call tree_walk_general(p(i)%x,vtree, &
                   (/peri*box_size,perj*box_size,perk*box_size/),u_sup) !tree.mod
            end do ; end do ;end do
          end if
      end select
    end if
    !normal fluid velocity
    call get_normal_velocity(p(i)%x,u_norm) !normal_fluid.mod
    u=u_sup+u_norm
  end subroutine
  !************************************************************
  !>diagnostics for fluid/inertial particles
  subroutine diagnostics_particles
    implicit none
    real :: uinfo(part_count,2)
    integer :: i
    uinfo(:,1)=sqrt(p(:)%u(1)**2+p(:)%u(2)**2+p(:)%u(3)**2)
    uinfo(:,2)=sqrt((p(:)%u1(1)-p(:)%u2(1))**2+&
                    (p(:)%u1(2)-p(:)%u2(2))**2+&
                    (p(:)%u1(3)-p(:)%u2(3))**2)
    !in here we need to determine
    part_maxu=maxval(uinfo(:,1)) ; part_maxdu=maxval(uinfo(:,2))
    part_urms=sqrt(sum(uinfo(:,1)**2)/part_count)
    !2 particle separation
    select case(initp)
      case('pairs')
        part_sep=0.
        do i=1, part_count, 2
          part_sep=part_sep+sqrt((p(i)%x(1)-p(i+1)%x(1))**2+&
                                 (p(i)%x(2)-p(i+1)%x(2))**2+&
                                 (p(i)%x(3)-p(i+1)%x(3))**2)
        end do
        part_sep=part_sep/(part_count/2)
    end select
    open(unit=78,file='data/par_ts.log',position='append')
    if (itime==shots) then
      write(78,*) '%-var--t-----maxu---maxdu----urms---part_sep--'
      if (particles_only) then
        !printing to screen
        write(*,*) '%-var--t-----maxu---maxdu----urms---part_sep--'
      end if
    end if
    write(78,'(i5.3,f8.2,f10.5,f10.5,f10.5,f10.5)') &
    itime/shots,t,part_maxu,part_maxdu,part_urms,part_sep
    if (particles_only) then
      write(*,'(i5.3,f8.2,f10.5,f10.5,f10.5,f10.5)') &
      itime/shots,t,part_maxu,part_maxdu,part_urms,part_sep
    end if
    close(78)
  end subroutine

  !*********************************************************
  !>traps the tracers on their nearest vortex point if they are close enough
  !brute-force it for now, no tree searching
  subroutine trap_particles()
    implicit none
    integer :: j, k
    real :: df, dpp1, dp_proj, dp_perp, d
    integer :: trap_index

    real :: x_proj(3), u_proj(3)
    
    !arbitrary large value
    p(:)%nearest_vp = 100.;

    !have to give up on openmp for now
    !to make the following code thread-safe basically entire thing
    !would have to be wrapped in $omp critical

    !trapping should be cheap anyway

    particles: do k=1, part_count
       !if the the particle is trapped, it's neartest_vp must be 0
       !don't even bother with looping over the vortices
       if(p(k)%trapping_vparticle > 0) then
          p(k)%nearest_vp = 0;
          p(k)%nearest_vi = p(k)%trapping_vparticle
          cycle
       end if

       do j=1, pcount
          if(f(j)%infront == 0) cycle

          d = dist_gen(f(j)%x, p(k)%x) !distance to f(j)

          !find the nearest vortex point
          if(p(k)%nearest_vp > d) then
             p(k)%nearest_vp = d;
             p(k)%nearest_vi = j;
          end if

          !trap if asked to do that and trap only if the vortex point
          !is free; that is, trapping on a 'filled' vortex point is
          !disabled, but the tracer particle is still affected by the
          !cutoff (otherwise we would end up with 1/0 most probably)
          if(tracer_trapping .and. f(j)%trapped_particle == 0) then
             dpp1 = dist_gen(f(j)%ghosti, p(k)%x); !distance to the next one

             trap_index = 0 !a guard value; don't trap if trap_index == 0

             !try to do the simple trapping
             !the scalar product is there to trap only if the particle is moving
             !towards the trapping point. This is to ensure that newly detrapped
             !particle is not immidately re-trapped
             if(d < tracer_trap_d .and. &
                  &dot_product(p(k)%u - f(j)%u&
                  &           ,p(k)%x - f(j)%x) < 0) then !trap on the f(j) vortex point

                trap_index = j

             else if (dpp1 < tracer_trap_d .and. &
                  &   f(f(j)%infront)%trapped_particle == 0 .and. &
                  &   dot_product(p(k)%u - f(f(j)%infront)%u&
                  &              ,p(k)%x - f(j)%ghosti) < 0) then !trap on the infront one

                trap_index = f(j)%infront

             else !do the general trapping
                df = dist_gen(f(j)%x, f(j)%ghosti)
                !projected distance along [f(j)%x, f(j)%ghosti] line
                dp_proj = dot_product(p(k)%x - f(j)%x,&
                     &                f(j)%ghosti - f(j)%x)/df

                x_proj = f(j)%x + (f(j)%ghosti - f(j)%x)*dp_proj/df
                u_proj = f(j)%u + (f(f(j)%infront)%u - f(j)%u)*dp_proj/df

                if(dp_proj > 0 .and. dp_proj < df .and. &
                     & dot_product(p(k)%x - x_proj,&
                     &             p(k)%u - u_proj) < 0) then 
                   !the point is in range of segment
                   
                   !the perpendicular distance to the segment
                   dp_perp = sqrt(d**2 - dp_proj**2)

                   if(dp_perp < tracer_trap_d) then !we need to add a new point
                      trap_index = pinsert_near_point(j, p(k)%x)
                   end if
                end if
             endif

             if(trap_index > 0) then
                !update the trapping bookkeeping
                f(trap_index)%trapped_particle = k
                p(k)%trapping_vparticle = trap_index

                !move the particle onto the vortex
                p(k)%x = f(trap_index)%x
                !and delete its history because the forces change discontinuously
                if(trapped_drag) then
                   p(k)%a3 = 0; p(k)%a2 = 0; p(k)%a1 = 0;
                   p(k)%u3 = 0; p(k)%u3 = 0; p(k)%u1 = 0;
                else
                   p(k)%u = f(trap_index)%u
                end if
                
                trapping_count = trapping_count + 1
                !we are finished with this particle
                cycle particles
             end if

          end if
       end do
    end do particles

  end subroutine trap_particles

end module
!>\page PARTICLES Fluid/Inertial particles
!!Initial condition for particles is set in run.in through the parameter initp.\n
!!Particle count is set in run.in through the parameter part_count.\n
!!Options are:\n
!!- \p one_side - All particles on one side of the box
!!\image html par_one_side_thumb.png
!!- \p random - random positions within the box
!!\image html par_random_thumb.png
!!- \p pairs - Particles in pairs randomly placed within the box, can be used to look at two-particle dispersion.
!!\image html par_pairs_thumb.png
!!
!!We can choose between particle type using the particle_count parameter in run.in, options available are:\n
!!
!!- \p fluid - Default option, massless particles \f${d\mathbf{x}_i}/{dt}=u(\mathbf{x}_i,t)\f$, the following plot shows the trajectories of 100 particles in the ABC flow:
!!\image html ABC_stokes_0_thumb.png 
!!- \p inertial - Particles have a mass therefore feel a stokes drag: 
!!\f[\frac{d\mathbf{x}_i}{dt}=\mathbf{u}_i, \qquad \frac{d\mathbf{u}_i}{dt}=\frac{u(\mathbf{x}_i,t)-\mathbf{u}_i}{\tau},\f] where \f$\tau\f$ is the stokes number. Note we define the reciprocal of the stokes number in run.in via the parameter part_stokes. The following plot shows the trajectories of 100 particles in the ABC flow with a stokes number 1:
!!\image html ABC_stokes_1_thumb.png
